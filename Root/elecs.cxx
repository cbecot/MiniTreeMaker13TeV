#ifndef ELECS_CXX
#define ELECS_CXX

#include "MiniTreeMaker13TeV/elecs.h"

// Constructor
elecs::elecs() {		
  return;
}
// Destructor
elecs::~elecs() {
}

//selection for the signal
int elecs::StandardElecSelection(xAOD::TEvent& m_event, const xAOD::EventInfo& eventInfo, xAOD::ElectronContainer* elcont, asg::AnaToolHandle<Trig::IMatchingTool> m_elTrigMatch){

  int NPV = 0; int NPUV = 0;
  const xAOD::VertexContainer* vertices = NULL;
  m_event.retrieve(vertices,"PrimaryVertices");
  xAOD::VertexContainer::const_iterator vitr;
  const xAOD::Vertex *pv(0);
  for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
    if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx) {
      NPV++;
      if(NPV==1) pv = (*vitr); 
    }
    if ( (*vitr)->vertexType() == xAOD::VxType::PileUp) NPUV++;   
  }
  if(!pv){
    Error("elecs::StandardElecSelection", "Failed to retrieve Primary Vertex. Exiting." );
    return -1;
  }
  const xAOD::ElectronContainer* elecs; 
  if ( !m_event.retrieve( elecs, "Electrons" ).isSuccess() )
  { // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Electrons container. Exiting." );
    return -1;
  }

  // create a shallow copy of the electrons container
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > elecs_shallowCopy = xAOD::shallowCopyContainer( *elecs );
  if(!xAOD::setOriginalObjectLink(*elecs, *(elecs_shallowCopy.first)))
  {//tell calib container what old container it matches
    std::cout << "Failed to set the original object links" << std::endl;
    return -1;
  }

  // iterate over our shallow copy
  xAOD::ElectronContainer::iterator elecSC_itr = (elecs_shallowCopy.first)->begin();
  xAOD::ElectronContainer::iterator elecSC_end = (elecs_shallowCopy.first)->end();
  //bool elTrigMatched =0;
  ngoodel = 0;
  for( ; elecSC_itr != elecSC_end; ++elecSC_itr ){
    (*elecSC_itr)-> auxdecor< bool >( "inrecoil") = false;
    //double index = (*elecSC_itr)->index();
    //m_egammaCalibrationAndSmearingTool->setRandomSeed(eventInfo.eventNumber() + 100 * index);
    if (m_egammaCalibrationAndSmearingTool->applyCorrection( **elecSC_itr ) == CP::CorrectionCode::Error)
    {// apply correction and check return code
      // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
      // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
      Error("execute()", "EgammaCalibrationAndSmearingTool returns Error CorrectionCode");
    }

    //bool el_id = m_electronLikelihoodToolLoose->accept((*elecSC_itr));
    bool el_isMedium = m_electronLikelihoodToolMedium->accept((*elecSC_itr));
    bool el_isTight = m_electronLikelihoodToolTight->accept((*elecSC_itr));
    bool el_isLoose = m_electronLikelihoodToolLoose->accept((*elecSC_itr));

    bool el_iso = m_isolationSelectionTool-> accept( **elecSC_itr);		//for the recoil
    const xAOD::TrackParticle *eltrk = (*elecSC_itr)->trackParticle();
    (*elecSC_itr)-> auxdata< bool >( "isTight") = el_isTight;
    (*elecSC_itr)-> auxdata< bool >( "isMedium") = el_isMedium;
    (*elecSC_itr)-> auxdata< bool >( "isLoose") = el_isLoose;

    (*elecSC_itr)-> auxdata< bool >( "isIsolated") = el_iso;
    float el_d0sig = 10.,el_deltaz0=10.,el_deltaz0new=10.;
    if(pv && eltrk){
      el_d0sig = xAOD::TrackingHelpers::d0significance( eltrk, eventInfo.beamPosSigmaX(), eventInfo.beamPosSigmaY(), eventInfo.beamPosSigmaXY() );
      el_deltaz0 = fabs(eltrk->z0() + eltrk->vz() - pv->z());
      el_deltaz0new = el_deltaz0 * sin(eltrk->theta());
      (*elecSC_itr)-> auxdata<float>( "deltaz0new" ) = el_deltaz0new;
      (*elecSC_itr)-> auxdata<float>( "d0sig" ) = el_d0sig;
    }
    bool isElVtxAssoc = (fabs(el_deltaz0new)<0.5) && (fabs(el_d0sig)<5.);
    //require :  ID (loose)
    //if (!el_id) continue;

    //maintaining both 7x11 and 3x7 clusters size
    if ((int) ((*elecSC_itr)->auxdata< int >("7x11ClusterLr2Size"))!= 77) continue;
    if ((int) ((*elecSC_itr)->auxdata< int >("3x7ClusterLr2Size"))!= 21) continue;
    //ensuring that the hottest cell is in the middle of the 7x11 cluster
    int maxElement = 0;
    float maxE = (*elecSC_itr)->auxdata<std::vector<float>>(Form("7x11ClusterLr2E"))[0];
    for (int k = 1; k < (77) ; k++) {
        if (maxE < (*elecSC_itr)->auxdata<std::vector<float>>(Form("7x11ClusterLr2E"))[k]) {
            maxE = (*elecSC_itr)->auxdata<std::vector<float>>(Form("7x11ClusterLr2E"))[k];
            maxElement = k;
            } 
         }
     if (maxElement!= 38) continue;
    //require : vertex association -- relaxed
    //if (!isElVtxAssoc) continue;
    //require : isolation -- relaxed
    if(!el_iso) continue;
    //require pT > 20 GeV + within acceptance
    if ( ! ((*elecSC_itr)->pt() > m_elecMinPt && fabs( (*elecSC_itr)->eta() ) < m_elecMaxEta ))    continue;
    if( fabs( (*elecSC_itr)->eta() ) > m_elecMinEtaCrack && fabs( (*elecSC_itr)->eta() ) < m_elecMaxEtaCrack )   continue;
    if(el_isTight && (*elecSC_itr)->pt() > m_elecFinalMinPt)//passes all requirements and is tight
      ngoodel++;
    double elrecosf = 1.0,  elllhsf_loose = 1.0,elllhsf_medium = 1.0, elisosf_medium = 1.0, elllhsf_tight = 1.0, elisosf_tight = 1.0, eltrigsf = 1.0, eltrigeff = 1.0;
    int id_rg_loose = 0, id_rg_medium = 0, iso_rg_medium = 0, id_rg_tight = 0,reco_rg = 0, trig_rg = 0; 	//regions for syst study 
    if(m_isMC)
    {
      m_elRecoCorr->getEfficiencyScaleFactor(**elecSC_itr,elrecosf);

      m_elLLHCorr_loose->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_loose);
      m_elLLHCorr_medium->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_medium);
      m_elIsoCorr_medium->getEfficiencyScaleFactor(**elecSC_itr,elisosf_medium);
      m_elLLHCorr_tight->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_tight);
      m_elIsoCorr_tight->getEfficiencyScaleFactor(**elecSC_itr,elisosf_tight);
      m_elTrigCorr->getEfficiencyScaleFactor(**elecSC_itr,eltrigsf);
      id_rg_loose  = m_elLLHCorr_loose->systUncorrVariationIndex(**elecSC_itr);

      id_rg_medium  = m_elLLHCorr_medium->systUncorrVariationIndex(**elecSC_itr);
      iso_rg_medium = m_elIsoCorr_medium->systUncorrVariationIndex(**elecSC_itr);
      id_rg_tight  = m_elLLHCorr_tight->systUncorrVariationIndex(**elecSC_itr);

      //useless...
      //iso_rg_tight = m_elIsoCorr_tight->systUncorrVariationIndex(**elecSC_itr);
      reco_rg = m_elRecoCorr->systUncorrVariationIndex(**elecSC_itr);
      trig_rg = m_elTrigCorr->systUncorrVariationIndex(**elecSC_itr);
      if((*elecSC_itr)->pt() < m_elecPtMaxTrig)
	m_elTrigEff->getEfficiencyScaleFactor(**elecSC_itr,eltrigeff);
    }
    (*elecSC_itr)->auxdata< double >( "isosf_medium" ) = (double)elisosf_medium;
    (*elecSC_itr)->auxdata< double >( "sfid_loose" ) = elllhsf_loose;
    (*elecSC_itr)->auxdata< double >( "sfid_medium" ) = elllhsf_medium;
    (*elecSC_itr)->auxdata< int >( "id_rg_medium" ) = id_rg_medium;
    (*elecSC_itr)->auxdata< int >( "iso_rg_medium" ) = iso_rg_medium;
    (*elecSC_itr)->auxdata< double >( "isosf_tight" ) = (double)elisosf_tight;
    (*elecSC_itr)->auxdata< double >( "sfid_tight" ) = elllhsf_tight;
    //totally useless
    //(*elecSC_itr)->auxdata< int >( "iso_rg_tight" ) = iso_rg_tight;
    (*elecSC_itr)->auxdata< double >( "recosf" ) = elrecosf;
    (*elecSC_itr)->auxdata< int >( "reco_rg" ) = reco_rg;
    (*elecSC_itr)->auxdata< int >( "trig_rg" ) = trig_rg;

    bool elTrMatch = 0;
    std::vector<const xAOD::IParticle*> myElecs(1,*elecSC_itr);
    if(!m_isMC)
    {
      for(TString str : electrigmatchnames) 
      {
	//non stdm4
	if(m_elTrigMatch->match( myElecs, str.Data() ))
	  elTrMatch = 1;
	//stdm4 : may be back later ??
	//TString trigname="STDM4_"+str;
	//if((*elecSC_itr)->auxdata< char >( trigname.Data() ))elTrMatch = 1;
      }
    }
    else
    {
      for(TString str : elecmctrigmatchnames) 
      {
	//non stdm4
	if(m_elTrigMatch->match( myElecs, str.Data() )) elTrMatch = 1;
	//stdm4 : may be back later ??
	//TString trigname="STDM4_"+str;
	//if((*elecSC_itr)->auxdata< char >( trigname.Data() ))elTrMatch = 1;
      }
    }  

    (*elecSC_itr)->auxdata< double >( "trigsf" ) = eltrigsf;
    (*elecSC_itr)->auxdata< float >( "trigmceff" ) = eltrigeff;
    (*elecSC_itr)->auxdata< bool >( "trigMatched" ) = elTrMatch;
    elcont->push_back(new xAOD::Electron(**elecSC_itr));


  } // end for loop over shallow copied electrons
  delete elecs_shallowCopy.first;
  delete elecs_shallowCopy.second;
  //probably have to uncomment
  //m_event.record( elcont, "goodElecs"  );
  //m_event.record( goodElecsAux, "goodElecsAux."  );
  
  //switch off final selection requirements
  //if( ngoodel == 0 )return 1;
  if(elcont->size() < 1 )
    return 1;
  //std::sort(goodElecs->begin(),goodElecs->end(),DescendingPt());
  return 0;

}

int elecs::AntiIsoElecSelection(xAOD::TEvent& m_event, const xAOD::EventInfo& eventInfo, xAOD::ElectronContainer* elcont, asg::AnaToolHandle<Trig::IMatchingTool> m_elTrigMatch){
  int NPV = 0; int NPUV = 0;
  const xAOD::VertexContainer* vertices = NULL;
  m_event.retrieve(vertices,"PrimaryVertices");
  xAOD::VertexContainer::const_iterator vitr;
  const xAOD::Vertex *pv(0);
  for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
    if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx) {
      NPV++;
      if(NPV==1) pv = (*vitr); 
    }
    if ( (*vitr)->vertexType() == xAOD::VxType::PileUp) NPUV++;   
  }
  if(!pv){
    Error("elecs::StandardElecSelection", "Failed to retrieve Primary Vertex. Exiting." );
    return -1;
  }
  const xAOD::ElectronContainer* elecs; 
  if ( !m_event.retrieve( elecs, "Electrons" ).isSuccess() )
  { // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Electrons container. Exiting." );
    return -1;
  }

  // create a shallow copy of the electrons container
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > elecs_shallowCopy = xAOD::shallowCopyContainer( *elecs );
  if(!xAOD::setOriginalObjectLink(*elecs, *(elecs_shallowCopy.first)))
  {//tell calib container what old container it matches
    std::cout << "Failed to set the original object links" << std::endl;
    return -1;
  }

  // iterate over our shallow copy
  xAOD::ElectronContainer::iterator elecSC_itr = (elecs_shallowCopy.first)->begin();
  xAOD::ElectronContainer::iterator elecSC_end = (elecs_shallowCopy.first)->end();
  //bool elTrigMatched =0;
  ngoodel = 0;
  int nsignalel = 0;
  int isel =-1;
  for( ; elecSC_itr != elecSC_end; ++elecSC_itr ) 
  {
    isel++;
    (*elecSC_itr)-> auxdecor< bool >( "inrecoil") = false;

    double index = (*elecSC_itr)->index();
    //m_egammaCalibrationAndSmearingTool->setRandomSeed(eventInfo.eventNumber() + 100 * index);
    if (m_egammaCalibrationAndSmearingTool->applyCorrection( **elecSC_itr ) == CP::CorrectionCode::Error)
    {// apply correction and check return code
      // Can have CorrectionCode values of Ok, OutOfValidityRange, or Error. Here only checking for Error.
      // If OutOfValidityRange is returned no modification is made and the original muon values are taken.
      Error("execute()", "EgammaCalibrationAndSmearingTool returns Error CorrectionCode");
    }

   // bool el_id = m_electronLikelihoodToolLoose->accept((*elecSC_itr));
    bool el_isTight = m_electronLikelihoodToolTight->accept((*elecSC_itr));
    bool el_iso = m_isolationSelectionTool-> accept( **elecSC_itr);
    const xAOD::TrackParticle *eltrk = (*elecSC_itr)->trackParticle();
    (*elecSC_itr)-> auxdata< bool >( "isTight") = el_isTight;
    float el_d0sig = 10.,el_deltaz0=10.,el_deltaz0new=10.;
    if(pv && eltrk){
      el_d0sig = xAOD::TrackingHelpers::d0significance( eltrk, eventInfo.beamPosSigmaX(), eventInfo.beamPosSigmaY(), eventInfo.beamPosSigmaXY() );
      el_deltaz0 = fabs(eltrk->z0() + eltrk->vz() - pv->z());
      el_deltaz0new = el_deltaz0 * sin(eltrk->theta());
      (*elecSC_itr)-> auxdata<float>( "deltaz0new" ) = el_deltaz0new;
      (*elecSC_itr)-> auxdata<float>( "d0sig" ) = el_d0sig;
    }
    bool isElVtxAssoc = (fabs(el_deltaz0new)<0.5) && (fabs(el_d0sig)<5.);
    //if (!el_id) continue;
    if (!isElVtxAssoc) continue;
    if ( ! ((*elecSC_itr)->pt() > m_elecMinPt && fabs( (*elecSC_itr)->eta() ) < m_elecMaxEta ))    continue;
    if( fabs( (*elecSC_itr)->eta() ) > m_elecMinEtaCrack && fabs( (*elecSC_itr)->eta() ) < m_elecMaxEtaCrack )   continue;
    if(el_iso){
      if((*elecSC_itr)->pt() > m_elecFinalMinPt && el_isTight) nsignalel++;			
      continue;
    }
    double elrecosf = 1.0,  elllhsf_loose = 1.0, elllhsf_medium = 1.0, elisosf_medium = 1.0, elllhsf_tight = 1.0, elisosf_tight = 1.0, eltrigsf = 1.0, eltrigeff = 1.0;
    int id_rg_loose = 0, id_rg_medium = 0, iso_rg_medium = 0, id_rg_tight = 0, reco_rg = 0, trig_rg = 0; 	//regions for syst study 
    if(m_isMC)
    {
      m_elRecoCorr->getEfficiencyScaleFactor(**elecSC_itr,elrecosf);
      m_elLLHCorr_loose->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_loose);

      m_elLLHCorr_medium->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_medium);
      m_elIsoCorr_medium->getEfficiencyScaleFactor(**elecSC_itr,elisosf_medium);
      m_elLLHCorr_tight->getEfficiencyScaleFactor(**elecSC_itr,elllhsf_tight);
      m_elIsoCorr_tight->getEfficiencyScaleFactor(**elecSC_itr,elisosf_tight);
      m_elTrigCorr->getEfficiencyScaleFactor(**elecSC_itr,eltrigsf);
     // id_rg_loose  = m_elLLHCorr_loose->systUncorrVariationIndex(**elecSC_itr);

      id_rg_medium  = m_elLLHCorr_medium->systUncorrVariationIndex(**elecSC_itr);
      iso_rg_medium = m_elIsoCorr_medium->systUncorrVariationIndex(**elecSC_itr);
      id_rg_tight  = m_elLLHCorr_tight->systUncorrVariationIndex(**elecSC_itr);
      //useless
      //iso_rg_tight = m_elIsoCorr_tight->systUncorrVariationIndex(**elecSC_itr);
      reco_rg = m_elRecoCorr->systUncorrVariationIndex(**elecSC_itr);
      trig_rg = m_elTrigCorr->systUncorrVariationIndex(**elecSC_itr);
      if((*elecSC_itr)->pt() < m_elecPtMaxTrig)
	m_elTrigEff->getEfficiencyScaleFactor(**elecSC_itr,eltrigeff);
    }
    (*elecSC_itr)->auxdata< double >( "isosf_medium" ) = (double)elisosf_medium;
    (*elecSC_itr)->auxdata< double >( "sfid_medium" ) = elllhsf_medium;
    (*elecSC_itr)->auxdata< double >( "sfid_loose" ) = elllhsf_loose;

    (*elecSC_itr)->auxdata< int >( "id_rg_medium" ) = id_rg_medium;
    (*elecSC_itr)->auxdata< int >( "iso_rg_medium" ) = iso_rg_medium;
    (*elecSC_itr)->auxdata< double >( "isosf_tight" ) = (double)elisosf_tight;
    (*elecSC_itr)->auxdata< double >( "sfid_tight" ) = elllhsf_tight;
    //totally useless
    //(*elecSC_itr)->auxdata< int >( "id_rg_tight" ) = id_rg_tight;
    //(*elecSC_itr)->auxdata< int >( "iso_rg_tight" ) = iso_rg_tight;
    (*elecSC_itr)->auxdata< double >( "recosf" ) = elrecosf;
    (*elecSC_itr)->auxdata< int >( "reco_rg" ) = reco_rg;
    (*elecSC_itr)->auxdata< int >( "trig_rg" ) = trig_rg;
    bool elTrMatch = 0;
    std::vector<const xAOD::IParticle*> myElecs(1,*elecSC_itr);
    if(!m_isMC)
    {
      for(TString str : electrigmatchnames) 
      {
	//non stdm4
	if(m_elTrigMatch->match( myElecs, str.Data() )) elTrMatch = 1;
	//stdm4
	//TString trigname="STDM4_"+str;
	//if((*elecSC_itr)->auxdata< char >( trigname.Data() ))elTrMatch = 1;
      }
    }
    else
    {
      for(TString str : elecmctrigmatchnames) 
      {
	//non stdm4
	if(m_elTrigMatch->match( myElecs, str.Data() )) elTrMatch = 1;
	//stdm4
	//TString trigname="STDM4_"+str;
	//if((*elecSC_itr)->auxdata< char >( trigname.Data() ))elTrMatch = 1;
      }
    }  

    (*elecSC_itr)->auxdata< double >( "trigsf" ) = eltrigsf;
    (*elecSC_itr)->auxdata< float >( "trigmceff" ) = eltrigeff;
    (*elecSC_itr)->auxdata< bool >( "trigMatched" ) = elTrMatch;
    elcont->push_back(new xAOD::Electron(**elecSC_itr));

    if((*elecSC_itr)->pt() > m_elecFinalMinPt && el_isTight)
      ngoodel++;
      //if(elTrMatch) elTrigMatched = 1;
    

  } // end for loop over shallow copied electrons
  delete elecs_shallowCopy.first;
  delete elecs_shallowCopy.second;
  //probably have to uncomment
  //m_event.record( elcont, "goodElecs"  );
  //m_event.record( goodElecsAux, "goodElecsAux."  );
  if(nsignalel > 0) {elcont->clear(); ngoodel =0;}	// veto event with signal elec, but maybe not so safe 
  if( ngoodel == 0 )return 1;

  //std::sort(goodElecs->begin(),goodElecs->end(),DescendingPt());
  return 0;

}

//selection for the signal
int elecs::ForwardElecSelection(xAOD::TEvent& m_event, xAOD::ElectronContainer* elcont){

  const xAOD::ElectronContainer* elecs; 
  if ( !m_event.retrieve( elecs, "ForwardElectrons" ).isSuccess() )
  { // retrieve arguments: container type, container key
    Error("execute()", "Failed to retrieve Forward Electrons container. Exiting." );
    return -1;
  }

  // create a shallow copy of the electrons container
  std::pair< xAOD::ElectronContainer*, xAOD::ShallowAuxContainer* > elecs_shallowCopy = xAOD::shallowCopyContainer( *elecs );
  if(!xAOD::setOriginalObjectLink(*elecs, *(elecs_shallowCopy.first)))
  {//tell calib container what old container it matches
    std::cout << "Failed to set the original object links" << std::endl;
    return -1;
  }

  // iterate over our shallow copy
  xAOD::ElectronContainer::iterator elecSC_itr = (elecs_shallowCopy.first)->begin();
  xAOD::ElectronContainer::iterator elecSC_end = (elecs_shallowCopy.first)->end();
  //bool elTrigMatched =0;
  for( ; elecSC_itr != elecSC_end; ++elecSC_itr ){
    //double index = (*elecSC_itr)->index();
    //m_egammaCalibrationAndSmearingTool->setRandomSeed(eventInfo.eventNumber() + 100 * index);
    //	if (m_egammaCalibrationAndSmearingTool->applyCorrection( **elecSC_itr ) == CP::CorrectionCode::Error)
    //  {
    //  	Error("execute()", "EgammaCalibrationAndSmearingTool returns Error CorrectionCode");
    //  }

    bool el_id = m_electronFwdIDTool->accept((*elecSC_itr));
    bool el_iso = m_isolationSelectionTool-> accept( **elecSC_itr);
    // no isolation available...
    //bool el_iso = true;
    (*elecSC_itr)-> auxdata<bool>( "Isolation" ) = el_iso;

    //if(!el_id) continue;
    if(!el_iso) continue;
    if ( ! ((*elecSC_itr)->pt() > m_elecMinFwdPt && fabs( (*elecSC_itr)->eta() ) < m_elecMaxFwdEta ))    continue;

    double elrecosf = 1.0,  elllhsf = 1.0, elisosf = 1.0;
    /*
       if(m_isMC)
       {
       m_elRecoCorr->getEfficiencyScaleFactor(**elecSC_itr,elrecosf);
       m_elLLHCorr->getEfficiencyScaleFactor(**elecSC_itr,elllhsf);
       m_elIsoCorr->getEfficiencyScaleFactor(**elecSC_itr,elisosf);
       }
       */
    (*elecSC_itr)->auxdata< double >( "isosf" ) = (double)elisosf;
    (*elecSC_itr)->auxdata< double >( "sfid" ) = elllhsf;
    (*elecSC_itr)->auxdata< double >( "recosf" ) = elrecosf;
    elcont->push_back(new xAOD::Electron(**elecSC_itr));

  } // end for loop over shallow copied electrons
  delete elecs_shallowCopy.first;
  delete elecs_shallowCopy.second;

  return 0;

}

void elecs::InitialiseElecTools(bool isMC) {
  m_isMC = isMC;

  m_elRecoCorr = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolReco");
  m_elLLHCorr_loose  = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolLLH_loose");
  m_elLLHCorr_medium  = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolLLH_medium");
  m_elIsoCorr_medium = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolIso_medium");
  m_elLLHCorr_tight  = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolLLH_tight");
  m_elIsoCorr_tight = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolIso_tight");
  m_elTrigCorr = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolTrig");
  m_elTrigEff = new AsgElectronEfficiencyCorrectionTool("ElectronMCEffToolTrig");
  m_elIsoVetoCorr = new AsgElectronEfficiencyCorrectionTool("ElectronEffCorrToolIsoVeto");

  m_elRecoCorr->setProperty("OutputLevel", MSG::INFO);
  m_elLLHCorr_loose->setProperty("OutputLevel", MSG::INFO);
  m_elLLHCorr_medium->setProperty("OutputLevel", MSG::INFO);
  m_elIsoCorr_medium->setProperty("OutputLevel", MSG::INFO);
  m_elLLHCorr_tight->setProperty("OutputLevel", MSG::INFO);
  m_elIsoCorr_tight->setProperty("OutputLevel", MSG::INFO);
  m_elTrigCorr->setProperty("OutputLevel", MSG::INFO);
  m_elTrigEff->setProperty("OutputLevel", MSG::INFO);
  m_elIsoVetoCorr->setProperty("OutputLevel", MSG::INFO);

  for(TString str : m_ElToolRecoCorrFilevec) 
    inputFile1.push_back(str.Data());
  for(TString str : m_ElToolLLHCorrFilevec_loose) 
    inputFile2_loose.push_back(str.Data());
  for(TString str : m_ElToolLLHCorrFilevec_medium) 
    inputFile2_medium.push_back(str.Data());
  for(TString str : m_ElToolIsoCorrFilevec_medium) 
    inputFile3_medium.push_back(str.Data());
  for(TString str : m_ElToolLLHCorrFilevec_tight) 
    inputFile2_tight.push_back(str.Data());
  for(TString str : m_ElToolIsoCorrFilevec_tight) 
    inputFile3_tight.push_back(str.Data());
  for(TString str : m_ElToolTrigCorrFilevec) 
    inputFile4.push_back(str.Data());
  for(TString str : m_ElToolTrigEffCorrFilevec) 
    inputFile5.push_back(str.Data());
  for(TString str : m_ElToolIsoVetoCorrFilevec) 
    inputFile6.push_back(str.Data());

  m_elRecoCorr->setProperty("CorrectionFileNameList",inputFile1);
  m_elRecoCorr->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elRecoCorr->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elRecoCorr->initialize();
  m_elLLHCorr_loose->setProperty("CorrectionFileNameList",inputFile2_loose);
  m_elLLHCorr_loose->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elLLHCorr_loose->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elLLHCorr_loose->initialize();
  m_elLLHCorr_medium->setProperty("CorrectionFileNameList",inputFile2_medium);
  m_elLLHCorr_medium->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elLLHCorr_medium->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elLLHCorr_medium->initialize();
  m_elIsoCorr_medium->setProperty("CorrectionFileNameList",inputFile3_medium);
  m_elIsoCorr_medium->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elIsoCorr_medium->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elIsoCorr_medium->initialize();
  m_elLLHCorr_tight->setProperty("CorrectionFileNameList",inputFile2_tight);
  m_elLLHCorr_tight->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elLLHCorr_tight->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elLLHCorr_tight->initialize();
  m_elIsoCorr_tight->setProperty("CorrectionFileNameList",inputFile3_tight);
  m_elIsoCorr_tight->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elIsoCorr_tight->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elIsoCorr_tight->initialize();
  m_elTrigCorr->setProperty("CorrectionFileNameList",inputFile4);
  m_elTrigCorr->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elTrigCorr->setProperty("CorrelationModel", EL_CorrModel.Data() );
  m_elTrigCorr->initialize();
  m_elTrigEff->setProperty("CorrectionFileNameList",inputFile5);
  m_elTrigEff->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elTrigEff->initialize();
  m_elIsoVetoCorr->setProperty("CorrectionFileNameList",inputFile6);
  m_elIsoVetoCorr->setProperty("ForceDataType",m_ElToolForcedataType);
  m_elIsoVetoCorr->initialize();

  m_egammaCalibrationAndSmearingTool = new CP::EgammaCalibrationAndSmearingTool("EgammaCalibrationAndSmearingTool");
  m_egammaCalibrationAndSmearingTool->setProperty("OutputLevel", MSG::INFO);

  if(! m_egammaCalibrationAndSmearingTool->setProperty("ESModel",m_ElToolCalibAndSmearESModel.Data()).isSuccess() ){
    Error("initialize()", "Failed to properly set property of the Electron calibration and smearing Tool. Exiting." );
    exit(10);
  }
  if(! m_egammaCalibrationAndSmearingTool->setProperty("decorrelationModel",m_ElToolCalibAndSmearDecorrModel.Data()).isSuccess() ){
    Error("initialize()", "Failed to properly set property of the Electron calibration and smearing Tool. Exiting." );
    exit(10);
  };
  if(! m_egammaCalibrationAndSmearingTool->initialize().isSuccess() ){
    Error("initialize()", "Failed to properly initialise Electron calibration and smearing Tool. Exiting." );
    exit(10);
  };

  //trkelver=new CP::TightTrackVertexAssociationTool("ElectronTrackVertexAssociationTool");
  //trkelver->setProperty("OutputLevel", MSG::INFO);

  m_electronLikelihoodToolLoose = new AsgElectronLikelihoodTool("ElectronLikelihoodToolLoose");
  m_electronLikelihoodToolLoose->setProperty("OutputLevel", MSG::FATAL);
  m_electronLikelihoodToolLoose->setProperty("primaryVertexContainer", m_ElToolLlhPVCont.Data());
  //	m_electronLikelihoodTool->setProperty("nPVdefault",0);
  //	m_electronLikelihoodTool->setProperty("usePVContainer",true);
 m_electronLikelihoodToolLoose->setProperty("ConfigFile",m_ElToolLlhLooseConfigFile.Data());
  m_electronLikelihoodToolLoose->initialize();
    m_electronLikelihoodToolMedium = new AsgElectronLikelihoodTool("ElectronLikelihoodToolMedium");
  m_electronLikelihoodToolMedium->setProperty("OutputLevel", MSG::FATAL);
  m_electronLikelihoodToolMedium->setProperty("primaryVertexContainer", m_ElToolLlhPVCont.Data());
  //	m_electronLikelihoodTool->setProperty("nPVdefault",0);
  //	m_electronLikelihoodTool->setProperty("usePVContainer",true);
  m_electronLikelihoodToolMedium->setProperty("ConfigFile",m_ElToolLlhMediumConfigFile.Data());
  m_electronLikelihoodToolMedium->initialize();
  m_electronLikelihoodToolTight = new AsgElectronLikelihoodTool("ElectronLikelihoodToolTight");
  m_electronLikelihoodToolTight->setProperty("OutputLevel", MSG::FATAL);
  m_electronLikelihoodToolTight->setProperty("primaryVertexContainer", m_ElToolLlhPVCont.Data());
  //	m_electronLikelihoodTool->setProperty("nPVdefault",0);
  //	m_electronLikelihoodTool->setProperty("usePVContainer",true);
  m_electronLikelihoodToolTight->setProperty("ConfigFile",m_ElToolLlhTightConfigFile.Data());
  m_electronLikelihoodToolTight->initialize();

  m_electronFwdIDTool = new AsgForwardElectronIsEMSelector("ElectronFwdisEM");
  m_electronFwdIDTool->setProperty("OutputLevel", MSG::FATAL);
  m_electronFwdIDTool->setProperty("ConfigFile",m_ElFwdIDConfigFile.Data());
  m_electronFwdIDTool->initialize();

  //for both mu and el
  m_isolationSelectionTool = new CP::IsolationSelectionTool("el_IsolationSelectionTool");
  m_isolationSelectionTool->setProperty("OutputLevel", MSG::INFO);
  //m_isolationSelectionTool->setProperty("MuonWP", m_IsolToolMuonWP.Data());
  m_isolationSelectionTool->setProperty("ElectronWP", m_IsolToolElecWP.Data());
  m_isolationSelectionTool->initialize();

}


#endif
