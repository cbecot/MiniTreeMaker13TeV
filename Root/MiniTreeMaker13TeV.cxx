#define MiniTreeMaker13TeV_cxx

/*
 *******
 * Code to slim/skim EGAM/MUON/STDM derivations and produce an output mini-tree
 * Author: Fabrice Balli <fabrice.balli@cern.ch>


*/

#include "MiniTreeMaker13TeV/MiniTreeMaker13TeV.h"

const float GeV = 1000.;
const char* APP_NAME = "Analysis";

bool descendingPt(xAOD::IParticle * a, xAOD::IParticle * b) { return a->pt() > b->pt(); }

float deltaPhi(float phi1, float phi2) {
  float dphi = std::fabs(phi1-phi2);
  if (dphi > TMath::Pi()) dphi = 2*TMath::Pi() - dphi;
  return dphi;
}

float deltaR(float eta1, float phi1, float eta2, float phi2) {
  float deta = eta1-eta2;	 
  float dphi = deltaPhi(phi1,phi2);		
  return sqrt(deta*deta+dphi*dphi);	 
}	
float deltaPhi(const xAOD::IParticle& j1, const xAOD::IParticle& j2) { 
  return TVector2::Phi_mpi_pi(j1.phi()-j2.phi());
}

// Constructor
MiniTreeMaker13TeV::MiniTreeMaker13TeV(void) :
  AsgMessaging("MiniTreeMaker13TeV"){
    return;
  }
// Destructor
MiniTreeMaker13TeV::~MiniTreeMaker13TeV() {}

void MiniTreeMaker13TeV::Initialise( xAOD::TEvent& event, TString daod ){
  initool = new initfinal();
  //initialise everything : tree, tools, object getter...
  initool -> initfinal::Initialise( event, settings_file, daod  );
  an = initool -> ana;
  m_isMC = initool->m_isMC;
  return;
}
// Per event analysis
void MiniTreeMaker13TeV::ProcessEvent(xAOD::TEvent& event) {
  //clear the vectors used to fill the tree
  an->mini->clearVec();
  const xAOD::EventInfo* eventInfo = 0;
  if( !event.retrieve( eventInfo, "EventInfo").isSuccess() ) 
    std::cout << " Failed to retrieve event info." << std::endl;
  //lumi block
  an->mini->lbn = eventInfo->lumiBlock();
  if (m_isMC)
    an->mini->mcChannelNumber = initool->m_mcChannelNumber;
    
  float wtgen = 1.;
  an -> nprocessed ++;
  if( m_isMC ){
    const std::vector< float > weights = eventInfo->mcEventWeights();
    if( weights.size() > 0 ){
      wtgen = weights[0];
      //this is not exactly right but ok...on average we have compensation
      //Powheg has weights equal to the cross section, so we want to avoid huge weights
      if(initool->isPowheg)wtgen=weights[0]/fabs(weights[0]);
      an -> wtgenPow += fabs(weights[0]);
      //for (int i =0;i < weights.size() ; i++) std::cout<< "mc event weight"<< i << "= " <<weights[i]<<std::endl;
    }
  }
  m_weight = m_isMC ? wtgen : 1.0;

  evtNbr = eventInfo->eventNumber();
  // average mu /bc

  (*an->mini->EventWeight)["gen"]=m_weight;
  an->mini->RunNumber= eventInfo->runNumber();
  an->mini->EvtNumber= evtNbr;
  //Produce a pileup decoration in EventInfo
  if(m_isMC&& initool->m_doAcc) {
    initool->mctruth->execute(event);
    an->getTruth(initool->mctruth);
    an->mini->RunNumber= eventInfo->runNumber();
    an->mini->EvtNumber= evtNbr;
    an->fillTree();
    initool->FinaliseSyst();
    return;
  }
  else {	
    float pileupwgh = 1.0;
    if(initool->m_pureweighting)  
      initool->m_PileupReweighting->apply(*eventInfo, true);
    else if(m_isMC) //need random run number for efficiencies etc...
      eventInfo->auxdecor< unsigned int >( "RandomRunNumber" )=282625;
    if(m_isMC && initool->m_pureweighting && eventInfo->auxdata< unsigned int >( "RandomRunNumber" ) ==0){
      an->FinaliseEvent(event);
      return;
    }
    if(m_isMC && initool->m_pureweighting)
      pileupwgh = eventInfo->auxdata< float >( "PileupWeight" ); 
    (*an->mini->EventWeight)["pu"]=pileupwgh;
    avmu = float(eventInfo->averageInteractionsPerCrossing());
    actmu = float(eventInfo->actualInteractionsPerCrossing());
    if(!m_isMC && initool->m_pureweighting){
      avmu = eventInfo->auxdata< float >( "corrected_averageInteractionsPerCrossing" ); 
      actmu = (initool->m_PileupReweighting )->getCorrectedActualInteractionsPerCrossing(*eventInfo,false);
      //actmu = avmu;
      //eventInfo->auxdata< float >( "corrected_actualInteractionsPerCrossing" ); 
    }

    m_weight *= pileupwgh;
    //Needed for muon trigger efficiency at 5 TeV -- may want to remove this as is a bit outside W mass...
    if(initool->m_do5TeV && m_isMC)
      eventInfo->auxdecor< unsigned int >( "RandomRunNumber" )=282625;
    an->m_cutFlow->Fill(Double_t (0), m_weight);

    // CUT: GRL for data only
    if (!initool->m_GRLPassThrough && !m_isMC && !initool->m_grlTool->passRunLB(*eventInfo) ) {
      an->FinaliseEvent(event);
      return;
    }
    an->m_cutFlow->Fill(1, m_weight);

    // CUT: LAr error
    // Event cleaning (for data only) to remove events with problematic regions of the detector
    if (!m_isMC && ( (eventInfo->errorState(xAOD::EventInfo::LAr)==xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::SCT)==xAOD::EventInfo::Error) ||
	  (eventInfo->errorState(xAOD::EventInfo::Tile)==xAOD::EventInfo::Error) ||
	  (eventInfo->isEventFlagBitSet(xAOD::EventInfo::Core, 18) ) ) ) {
      an->FinaliseEvent(event);
      return;
    }
    an->m_cutFlow->Fill(2, m_weight);

    const xAOD::VertexContainer* vertices = NULL;
    event.retrieve(vertices,"PrimaryVertices");

    int NPV = 0; int NPUV = 0;
    xAOD::VertexContainer::const_iterator vitr;
    for (vitr = vertices->begin(); vitr != vertices->end(); ++vitr){
      if ( (*vitr)->vertexType() == xAOD::VxType::PriVtx) {
	NPV++;
	if(NPV==1) pv = (*vitr); 
      }
      if ( (*vitr)->vertexType() == xAOD::VxType::PileUp) NPUV++;   
    }
    if(!initool->m_PassThrough && NPV ==0){an->FinaliseEvent(event); return;}
    an->m_cutFlow->Fill(3, m_weight);


    //an->mini->EventWeight["tot"]=m_weight;
    an->mini->bcid = eventInfo->bcid();
    an->mini->RunNumber= eventInfo->runNumber();
    an->mini->EvtNumber= evtNbr;
    an->mini->actual_mu= actmu;
    an->mini->npv= NPV+NPUV;
    if(NPV==1)
      an->mini->vxp_z0 = pv -> z(); 
    else
      an->mini->vxp_z0 = -9999;


    /////////////////////////////////////////////
    // Apply systematic uncertainty variations //
    /////////////////////////////////////////////

    an->m_cutFlow->Fill(4, m_weight);
    //unsigned int m_channel = 9999;
    bool isel = false;
    //bool ismu = false;
    //bool isjt = false; //useless boolean
    if(initool->m_DEBUG)std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    if(NPV!=0){
      if(! initool->m_antiIso){
	if(initool->eltool->StandardElecSelection(event,  *eventInfo, an->goodElecs, initool->trigtool-> m_elTrigMatch) == 0 ) isel = true;
	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
	//	if(initool->mutool->StandardMuonSelection(event,  *eventInfo, an->goodMuons, an->inclMuons, initool->trigtool-> m_muTrigMatch) == 0 ) ismu = true;
	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
	//	if(initool->jettool->StandardJetSelection(event, an->goodJets) == 0 )  isjt = true;
	if(initool->m_DEBUG) 
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
      }
    }
    else{
      if(initool->eltool->AntiIsoElecSelection(event,  *eventInfo, an->goodElecs, initool->trigtool-> m_elTrigMatch) == 0 ) isel = true;
     // if(initool->mutool->AntiIsoMuonSelection(event,  *eventInfo, an->goodMuons, an->inclMuons, initool->trigtool-> m_muTrigMatch) == 0 ) ismu = true;
     // if(initool->jettool->StandardJetSelection(event, an->goodJets) == 0 )  isjt = true;
    }
    	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

    ngoodel = initool->eltool->ngoodel;
    ngoodmu = initool->mutool->ngoodmu;
    //forward electrons, this is for hadronic recoil related studies -- have to see if really useful
    //    initool->eltool->ForwardElecSelection(event, an->goodFwdElecs);
    //if(initool->eltool->ForwardElecSelection(event, an->goodFwdElecs) != 0 ) isel = true;
	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

    //auto& orTool = toolBox.masterTool;
	if(!initool->m_PassThrough && !isel /*&& !ismu*/){
      initool->FinaliseSyst();
      return;
    }
    	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

    // no requirement on lepton of different flavor
    //if(initool->eltool->ngoodel > 0 && initool->mutool->ngoodmu > 0){initool->FinaliseSyst(); continue;}
    an->m_cutFlow->Fill(5, m_weight);
	if(initool->m_DEBUG)
	  std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

	initool->trigtool->getTriggerDecision();
	an->mini->passTrigger_el = initool->trigtool->passTrigger_el;
	an->mini->passTrigger_mu = initool->trigtool->passTrigger_mu;

    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

    //Recoil part
    //    if(NPV!=0)
    //  fillRecoilVariables(isel, ismu, event);
    
    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    //truth info for STDM derivations only
    if(m_isMC && initool->m_doTruth){
      initool->mctruth->execute(event);
      an->getTruth(initool->mctruth);
    }
    // fill jet variables
    //  if(initool->m_DEBUG)
    //   std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    // for(unsigned int i = 0 ; i<an->goodJets->size(); i++){
    //  an->mini->jet_pt->push_back((an->goodJets->at(i))->pt());
    //  an->mini->jet_eta->push_back((an->goodJets->at(i))->eta());
    //  an->mini->jet_phi->push_back((an->goodJets->at(i))->phi());
    //    }

    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    //fillInclMuonsVariables();
    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    //fillMuonsVariables();
    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    fillElectronsVariables();
      
    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    an->fillTree();
    if(initool->m_DEBUG)
      std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
    //Forward electrons : for later
    /*
    for(unsigned int i = 0 ; i<an->goodFwdElecs->size(); i++){
      //comment for the moment, will see later on if it is interesting
      if(sysname=="nominal")
	{
	an->fwdel_pt = an->goodFwdElecs->at(i)->pt();
	an->fwdel_eta = an->goodFwdElecs->at(i)->eta();
	an->fwdel_phi = an->goodFwdElecs->at(i)->phi();
	an->fwdel_sf = (float)(an->goodFwdElecs->at(i))->auxdata< float >( "EfficiencyScaleFactor" );
	}
    }
    */
  }//end if doAcc, else
  //an->FinaliseEvent(event);
  initool->FinaliseSyst();
  return;
  //std::cout<<"Ended loop over systematics"<<std::endl;
}

void MiniTreeMaker13TeV::SelectRecoilLeptons(xAOD::IParticleContainer* init, xAOD::IParticleContainer* initadd, bool init_isel){

  if(initadd->size() < 1 ){
    if(init_isel){
      xAOD::IParticleContainer::iterator p_itr = init->begin();
      xAOD::IParticleContainer::iterator p_end = init->end();
      for( ; p_itr != p_end;){
	if((*p_itr)->pt()  < initool->m_elecMinRecoilPt ){ //pt > XX MeV (default : 18000)
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	if(! ((*p_itr)-> auxdata< bool >( "isTight"))  && initool->m_elecRecoilTight ){  //require : Tight (?)
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	if(! ((*p_itr)-> auxdata< bool >( "isMedium"))  && initool->m_elecRecoilMedium ){ //require : Medium (?)
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	if(! ((*p_itr)-> auxdata< bool >( "isIsolated")) ){ //require : isolation 
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	float z0 = (*p_itr)-> auxdata<float>( "deltaz0new" );
	float d0 = (*p_itr)-> auxdata<float>( "d0sig" );
	bool isElVtxAssoc = (fabs(z0)<0.5) && (fabs(d0)<5.);
	if(!isElVtxAssoc){ // require : vertex association
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	++p_itr;
      }
    }
    else{
      xAOD::IParticleContainer::iterator p_itr = init->begin();
      xAOD::IParticleContainer::iterator p_end = init->end();
      for( ; p_itr != p_end;){
	if((*p_itr)->pt()  < initool->m_muonMinRecoilPt ){ //pt > XX MeV (default : 18000)
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	if(! ((*p_itr)-> auxdata< bool >( "isTight"))  && initool->m_muonRecoilTight ){  //require : Tight (?)
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	if(! ((*p_itr)-> auxdata< bool >( "isIsolated")) ){ //require : isolation 
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	float z0 = (*p_itr)-> auxdata<float>( "deltaz0new" );
	float d0 = (*p_itr)-> auxdata<float>( "d0sig" );
	bool isMuVtxAssoc = (fabs(z0)<0.5) && (fabs(d0)<3.);
	if(!isMuVtxAssoc){ // require : vertex association
	  init->erase(p_itr);
	  p_end--;
	  continue;
	}
	++p_itr;
      }
    }
  }
  else{
    xAOD::IParticleContainer::iterator p_itr = init->begin();
    xAOD::IParticleContainer::iterator p_end = init->end();
    for( ; p_itr != p_end;){
      if((*p_itr)->pt()  < initool->m_elecMinRecoilPt ){ //pt > XX MeV (default : 18000)
	init->erase(p_itr);
	p_end--;
	continue;
      }
      if(! ((*p_itr)-> auxdata< bool >( "isTight"))  && initool->m_elecRecoilTight ){  //require : Tight (?)
	init->erase(p_itr);
	p_end--;
	continue;
      }
      if(! ((*p_itr)-> auxdata< bool >( "isMedium"))  && initool->m_elecRecoilMedium ){ //require : Medium (?)
	init->erase(p_itr);
	p_end--;
	continue;
      }
      if(! ((*p_itr)-> auxdata< bool >( "isIsolated")) ){ //require : isolation 
	init->erase(p_itr);
	p_end--;
	continue;
      }
      float z0 = (*p_itr)-> auxdata<float>( "deltaz0new" );
      float d0 = (*p_itr)-> auxdata<float>( "d0sig" );
      bool isElVtxAssoc = (fabs(z0)<0.5) && (fabs(d0)<5.);
      if(!isElVtxAssoc){ // require : vertex association
	init->erase(p_itr);
	p_end--;
	continue;
      }
      ++p_itr;
    }

    p_itr = initadd->begin();
    p_end = initadd->end();
    for( ; p_itr != p_end; ++p_itr){
      if((*p_itr)->pt()  < initool->m_muonMinRecoilPt ) //pt > XX MeV (default : 18000)
	continue;
      if(! ((*p_itr)-> auxdata< bool >( "isTight"))  && initool->m_muonRecoilTight )  //require : Tight (?)
	continue;
      if(! ((*p_itr)-> auxdata< bool >( "isIsolated")) ) //require : isolation 
	continue;
      float z0 = (*p_itr)-> auxdata<float>( "deltaz0new" );
      float d0 = (*p_itr)-> auxdata<float>( "d0sig" );
      bool isMuVtxAssoc = (fabs(z0)<0.5) && (fabs(d0)<3.);
      if(!isMuVtxAssoc) // require : vertex association
	continue;
      xAOD::IParticle *tempPart = (*p_itr);
      init->push_back((tempPart));
    }

  }
  return;
}



void MiniTreeMaker13TeV::fillElectronsVariables(){
  an->mini->n_electrons = an->goodElecs->size();
  std::vector<CP::SystematicSet>::const_iterator sysListItr;
  for(unsigned int i = 0 ; i<an->goodElecs->size(); i++){
      an->mini->el_pt->push_back((an->goodElecs->at(i))->pt());
      an->mini->el_eta->push_back((an->goodElecs->at(i))->eta());
      an->mini->el_phi->push_back((an->goodElecs->at(i))->phi());
      float  eltrigsf = (float)(an->goodElecs->at(i))->auxdata< double >( "trigsf" );
      an->mini->el_isTight->push_back((an->goodElecs->at(i))->auxdata< bool >( "isTight") );
      an->mini->el_isMedium->push_back((an->goodElecs->at(i))->auxdata< bool >( "isMedium") );
      an->mini->el_isLoose->push_back((an->goodElecs->at(i))->auxdata< bool >( "isLoose") );

      an->mini->el_medium_idsf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "sfid_medium" ));
      an->mini->el_medium_isosf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "isosf_medium" ));
      an->mini->el_tight_idsf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "sfid_tight" ));
      an->mini->el_tight_isosf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "isosf_tight" ));
      an->mini->el_loose_idsf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "sfid_loose" ));

      an->mini->el_recosf->push_back((float)(an->goodElecs->at(i))->auxdata< double >( "recosf" ));
      an->mini->el_charge->push_back((an->goodElecs->at(i))->charge());
      an->mini->el_trigMatch->push_back((an->goodElecs->at(i))->auxdata< bool >( "trigMatched" ));
      an->mini->el_inrecoil->push_back((an->goodElecs->at(i))->auxdata< bool >( "inrecoil" ));
      an->mini->el_trigmceff->push_back((float)(an->goodElecs->at(i))->auxdata< float >( "trigmceff" ));
      an->mini->el_trigsf->push_back(eltrigsf);
      std::map<std::string, float> elisomap;
      if(initool->m_DEBUG)std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

      //----------------------------------------------------------------------------------
      // Energy cluster cells manipulations
     
           fillCaloCluster (i); // a 7x11 cluser in eta*phi coordinates, i-th electron in the event
	         
      //----------------------------------------------------------------------------------
      


      elisomap["ptvarcone20"] = (an->goodElecs->at(i))->auxdata< float >( "ptvarcone20" );
      elisomap["ptvarcone30"] = (an->goodElecs->at(i))->auxdata< float >( "ptvarcone30" );
      elisomap["ptvarcone40"] = (an->goodElecs->at(i))->auxdata< float >( "ptvarcone40" );

      elisomap["topoetcone20"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone20" );
      elisomap["topoetcone30"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone30" );
      elisomap["topoetcone40"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone40" );

      elisomap["topoetcone20ptCorrection"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone20ptCorrection" );
      elisomap["topoetcone30ptCorrection"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone30ptCorrection" );
      elisomap["topoetcone40ptCorrection"] = (an->goodElecs->at(i))->auxdata< float >( "topoetcone40ptCorrection" );
      if(initool->m_DEBUG)std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

      an->mini->el_isolation->push_back(elisomap);
      an->mini->el_d0sig->push_back((an->goodElecs->at(i))->auxdata< float >( "d0sig" ));
      an->mini->el_delta_z0->push_back((an->goodElecs->at(i))->auxdata< float >( "deltaz0new" ));
      if(initool->m_DEBUG)std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;

      if(initool->m_dosysEL && m_isMC){
	std::map<std::string, float>  mapelcalib;
	double ptel = (an->goodElecs->at(i))->pt();
	for (sysListItr = initool->m_sysListCB.begin(); sysListItr != initool->m_sysListCB.end(); ++sysListItr){
	  std::string sysname = (*sysListItr).name();
	  CP::SystematicVariation sysvar(sysname);
	  // elec calibration systematics	
	  if((initool->eltool)->m_egammaCalibrationAndSmearingTool->isAffectedBySystematic(sysvar)){
	    if( (initool->eltool)->m_egammaCalibrationAndSmearingTool->applySystematicVariation( *sysListItr ) != CP::SystematicCode::Ok ){
	      Error("execute()", "Cannot configure electron calibration tool for systematic" );
	      initool->FinaliseSyst();
	      return;
	    }    
	    if (initool->eltool->m_egammaCalibrationAndSmearingTool->applyCorrection( *(an->goodElecs->at(i)) ) == CP::CorrectionCode::Error){// apply correction and check return code
	      Error("execute()", "EgammaCalibrationAndSmearingTool returns Error CorrectionCode");
	    }

	    if(ptel != (an->goodElecs->at(i))->pt() ){
	      //std::cout << sysname << std::endl;
	      mapelcalib[sysname]=(an->goodElecs->at(i))->pt();
	    }
	  }
	} // end loop on elec calib syst
	an->mini->el_syspt_vec->push_back(mapelcalib);
	//reset calib to nominal
	(initool->eltool)->m_egammaCalibrationAndSmearingTool->applySystematicVariation( CP::SystematicSet()  ); 
	initool->eltool->m_egammaCalibrationAndSmearingTool->applyCorrection(*(an->goodElecs->at(i)));

	if(initool->m_DEBUG)std::cout << "function : "<< __FUNCTION__ <<", line = " << __LINE__ <<std::endl;
	//SF uncertainties : correlated NPs
	std::map<std::string, float> maprecosf, mapllhsf_medium, mapisosf_medium, mapllhsf_tight, mapisosf_tight, maptrigsf, maptrigmceff;
	for (auto sysListItr2 = (*initool->m_sysListSF_corr)["m_elLLHCorr"]->begin(); sysListItr2 != (*initool->m_sysListSF_corr)["m_elLLHCorr"]->end(); ++sysListItr2){
	  double idsf2 = 1.0;
	  std::string sysname2 = (*sysListItr2).name();
    initool->eltool->m_elLLHCorr_loose->applySystematicVariation( *sysListItr2 );
    initool->eltool->m_elLLHCorr_loose->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
    mapllhsf_medium[sysname2]=idsf2;

	  initool->eltool->m_elLLHCorr_medium->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elLLHCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	  mapllhsf_medium[sysname2]=idsf2;
	  initool->eltool->m_elLLHCorr_tight->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elLLHCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	  mapllhsf_tight[sysname2]=idsf2;
	}
	for (auto sysListItr2 = (*initool->m_sysListSF_corr)["m_elIsoCorr"]->begin(); sysListItr2 != (*initool->m_sysListSF_corr)["m_elIsoCorr"]->end(); ++sysListItr2){
	  double isosf2 = 1.0;
	  std::string sysname2 = (*sysListItr2).name();
	  initool->eltool->m_elIsoCorr_medium->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elIsoCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	  mapisosf_medium[sysname2]=isosf2;
	  initool->eltool->m_elIsoCorr_tight->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elIsoCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	  mapisosf_tight[sysname2]=isosf2;
	}
	for (auto sysListItr2 = (*initool->m_sysListSF_corr)["m_elRecoCorr"]->begin(); sysListItr2 != (*initool->m_sysListSF_corr)["m_elRecoCorr"]->end(); ++sysListItr2){
	  std::string sysname2 = (*sysListItr2).name();
	  double recosf2 = 1.0;
	  initool->eltool->m_elRecoCorr->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elRecoCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),recosf2);
	  maprecosf[sysname2]=recosf2;
	}
	for (auto sysListItr2 = (*initool->m_sysListSF_corr)["m_elTrigCorr"]->begin(); sysListItr2 != (*initool->m_sysListSF_corr)["m_elTrigCorr"]->end(); ++sysListItr2){
	  std::string sysname2 = (*sysListItr2).name();
	  double eltrigsf2 = 1.0;
	  initool->eltool->m_elTrigCorr->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elTrigCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),eltrigsf2);
	  maptrigsf[sysname2]=eltrigsf2;
	  initool->eltool->m_elTrigEff->applySystematicVariation( *sysListItr2 );
	  initool->eltool->m_elTrigEff->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),eltrigsf2);
	  maptrigmceff[sysname2]=eltrigsf2;
	}
	//Uncorrelated part : statistical only, only need to lookup the 'region' (= bin number) for each electron
	int idrg=(an->goodElecs->at(i))->auxdata< int >( "id_rg_medium" );
	int isorg=(an->goodElecs->at(i))->auxdata< int >( "iso_rg_medium" );
	int recorg=(an->goodElecs->at(i))->auxdata< int >( "reco_rg" );
	int trigrg=(an->goodElecs->at(i))->auxdata< int >( "trig_rg" );

	double idsf2 = 1.0;
	std::string sysname_down = Form("EL_EFF_ID_FULL_UncorrUncertaintyNP%d__1down",idrg);
	std::string sysname_up = Form("EL_EFF_ID_FULL_UncorrUncertaintyNP%d__1up",idrg);

  initool->eltool->m_elLLHCorr_loose->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_down))->second));
  initool->eltool->m_elLLHCorr_loose->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
  mapllhsf_medium[sysname_down]=idsf2;
  initool->eltool->m_elLLHCorr_loose->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_up))->second));
  initool->eltool->m_elLLHCorr_loose->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
  mapllhsf_medium[sysname_up]=idsf2;

	initool->eltool->m_elLLHCorr_medium->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_down))->second));
	initool->eltool->m_elLLHCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	mapllhsf_medium[sysname_down]=idsf2;
	initool->eltool->m_elLLHCorr_medium->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_up))->second));
	initool->eltool->m_elLLHCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	mapllhsf_medium[sysname_up]=idsf2;
	//std::cout << "reco sys" << sysname_down << std::endl;
	//std::cout << "idsfm" << idsf2 << std::endl;
	initool->eltool->m_elLLHCorr_tight->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_down))->second));
	initool->eltool->m_elLLHCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	mapllhsf_tight[sysname_down]=idsf2;
	initool->eltool->m_elLLHCorr_tight->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elLLHCorr"])->find(sysname_up))->second));
	initool->eltool->m_elLLHCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),idsf2);
	mapllhsf_tight[sysname_up]=idsf2;
	//std::cout << "reco sys" << sysname_down << std::endl;
	//std::cout << "idsft" << idsf2 << std::endl;

	double isosf2 = 1.0;
	sysname_down = Form("EL_EFF_Iso_FULL_UncorrUncertaintyNP%d__1down",isorg);
	sysname_up = Form("EL_EFF_Iso_FULL_UncorrUncertaintyNP%d__1up",isorg);
	initool->eltool->m_elIsoCorr_medium->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elIsoCorr"])->find(sysname_down))->second));
	initool->eltool->m_elIsoCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	mapisosf_medium[sysname_down]=isosf2;
	initool->eltool->m_elIsoCorr_medium->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elIsoCorr"])->find(sysname_up))->second));
	initool->eltool->m_elIsoCorr_medium->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	mapisosf_medium[sysname_up]=isosf2;
	initool->eltool->m_elIsoCorr_tight->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elIsoCorr"])->find(sysname_down))->second));
	initool->eltool->m_elIsoCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	mapisosf_tight[sysname_down]=isosf2;
	initool->eltool->m_elIsoCorr_tight->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elIsoCorr"])->find(sysname_up))->second));
	initool->eltool->m_elIsoCorr_tight->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),isosf2);
	mapisosf_tight[sysname_up]=isosf2;

	double recosf2 = 1.0;
	sysname_down = Form("EL_EFF_Reco_FULL_UncorrUncertaintyNP%d__1down",recorg);
	sysname_up = Form("EL_EFF_Reco_FULL_UncorrUncertaintyNP%d__1up",recorg);
	//std::cout<< "index = " << recorg << std::endl;
	initool->eltool->m_elRecoCorr->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elRecoCorr"])->find(sysname_down))->second));
	initool->eltool->m_elRecoCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),recosf2);
	maprecosf[sysname_down]=recosf2;
	initool->eltool->m_elRecoCorr->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elRecoCorr"])->find(sysname_up))->second));
	initool->eltool->m_elRecoCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),recosf2);
	maprecosf[sysname_up]=recosf2;

	double trigsf2 = 1.0;
	sysname_down = Form("EL_EFF_Trigger_FULL_UncorrUncertaintyNP%d__1down",trigrg);
	sysname_up = Form("EL_EFF_Trigger_FULL_UncorrUncertaintyNP%d__1up",trigrg);
	initool->eltool->m_elTrigCorr->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elTrigCorr"])->find(sysname_down))->second));
	initool->eltool->m_elTrigCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),trigsf2);
	maptrigsf[sysname_down]=trigsf2;
	initool->eltool->m_elTrigEff->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elTrigCorr"])->find(sysname_down))->second));
	initool->eltool->m_elTrigEff->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),trigsf2);
	maptrigmceff[sysname_down]=trigsf2;
	initool->eltool->m_elTrigCorr->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elTrigCorr"])->find(sysname_up))->second));
	initool->eltool->m_elTrigCorr->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),trigsf2);
	maptrigsf[sysname_up]=trigsf2;
	initool->eltool->m_elTrigEff->applySystematicVariation(*((((*initool->m_sysListSF_uncorr)["m_elTrigCorr"])->find(sysname_up))->second));
	initool->eltool->m_elTrigEff->getEfficiencyScaleFactor(*(an->goodElecs->at(i)),trigsf2);
	maptrigmceff[sysname_up]=trigsf2;
	//now, push all this (correlated + uncorrelated) into the electron vector of systematic maps
	an->mini->el_sys_medium_idsf_vec->push_back(mapllhsf_medium);
	an->mini->el_sys_medium_isosf_vec->push_back(mapisosf_medium);
	an->mini->el_sys_tight_idsf_vec->push_back(mapllhsf_tight);
	an->mini->el_sys_tight_isosf_vec->push_back(mapisosf_tight);
	an->mini->el_sysrecosf_vec->push_back(maprecosf);	
	an->mini->el_systrigsf_vec->push_back(maptrigsf);
	an->mini->el_systrigmceff_vec->push_back(maptrigmceff);			

	

      }
    }//end loop on selected electrons
}



void MiniTreeMaker13TeV::fillCaloCluster (int i) {
  
  //------------------------------------------------------------------    
  /* 
  float clusterSumE (int minEta, int maxEta, int minPhi, int maxPhi) {
    
	for ( int eta = minEta; eta < maxEta; eta ++ ) {
	  float sumE = 0;  
	  //summing energy over current eta column                                                                       
	  for ( int phi = minPhi; phi < maxPhi; phi ++ ) {
	    sumE += clusterCellsE[phi+11*eta];
	  }
	}
	return sumE;
  }

  */
  //------------------------------------------------------------------          
  
  bool debug =0;

  
  an->mini->el_weta1->push_back((float)(an->goodElecs->at(i))->auxdata<float>("weta1"));
  an->mini->el_weta2->push_back((float)(an->goodElecs->at(i))->auxdata<float>("weta2"));
  an->mini->el_Reta->push_back((float)(an->goodElecs->at(i))->auxdata<float>("Reta"));
  an->mini->el_Rphi->push_back((float)(an->goodElecs->at(i))->auxdata<float>("Rphi"));
  an->mini->el_Eratio->push_back((float)(an->goodElecs->at(i))->auxdata<float>("Eratio"));
  //an->mini->el_f1->push_back((float)(an->goodElecs->at(i))->auxdata<float>("f1"));
  an->mini->el_f3->push_back((float)(an->goodElecs->at(i))->auxdata<float>("f3"));
  an->mini->el_ClusterSize7x11Lr2->push_back((int)(an->goodElecs->at(i))->auxdata<int>("7x11ClusterLr2Size"));
  an->mini->el_ClusterSize3x7Lr2->push_back((int)(an->goodElecs->at(i))->auxdata<int>("3x7ClusterLr2Size"));

  

  auto clusters = xAOD::EgammaHelpers::getAssociatedTopoClusters(an->goodElecs->at(i)->caloCluster());
 int ntopos;
 // auto ntopos = clusters.size;
  ntopos = clusters.size();
 
  if (debug) {
  std::cout << "number of clusters: " << ntopos << std::endl; 
}    

    an->mini->el_topoClustersNumber->push_back((int)(ntopos));
	
	  std::vector< float> topoClustersE;
    std::vector< float> topoClustersEta;
    std::vector< float> topoClustersPhi;
    std::vector< float> topoClustersPt;
//calE calEta calPhi calM

	         for (int n = 0; n < ntopos; n++) {

	      if (clusters[n]!=0) {
  if (debug) {
               std::cout << "Cluster number : " << n << " address " << clusters[n]<< std::endl; 

        std::cout <<     clusters[n]->isAvailable<float>("calE")  << std::endl;

          std::cout << "Cluster number : " << n << " energy " << clusters[n]->auxdata<float>("calE")<< std::endl; 



          std::cout << "Cluster number : " << n << " pt " <<  xAOD::EgammaHelpers::getAssociatedTopoClusters(an->goodElecs->at(i)->caloCluster())[n]->pt()<< std::endl; 
	         std::cout << "Cluster number : " << n << " eta " << xAOD::EgammaHelpers::getAssociatedTopoClusters(an->goodElecs->at(i)->caloCluster())[n]->eta()<< std::endl; 
	        std::cout << "Cluster number : " << n << " phi " << xAOD::EgammaHelpers::getAssociatedTopoClusters(an->goodElecs->at(i)->caloCluster())[n]->phi()<< std::endl; 
          std::cout << "Cluster number : " << n << " nsamples " << xAOD::EgammaHelpers::getAssociatedTopoClusters(an->goodElecs->at(i)->caloCluster())[n]->nSamples()<< std::endl; 
}
	         topoClustersE.push_back(clusters[n]->e());
	         topoClustersEta.push_back(clusters[n]->eta());
	         topoClustersPhi.push_back(clusters[n]->phi());
	         topoClustersPt.push_back(clusters[n]->pt());
      }
      else continue;
				}


    an->mini->el_topoClustersE->push_back(topoClustersE);
    an->mini->el_topoClustersEta->push_back(topoClustersEta);
    an->mini->el_topoClustersPhi->push_back(topoClustersPhi);
    an->mini->el_topoClustersPt->push_back(topoClustersPt);

  auto  clusterCellsE = (an->goodElecs->at(i))->auxdata<std::vector<float>>(Form("7x11ClusterLr2E"));
  auto  clusterCellsEta = (an->goodElecs->at(i))->auxdata<std::vector<float>>(Form("7x11ClusterLr2Eta"));
  auto  clusterCellsE37 = (an->goodElecs->at(i))->auxdata<std::vector<float>>(Form("3x7ClusterLr2E"));
  auto  clusterCellsEta37 = (an->goodElecs->at(i))->auxdata<std::vector<float>>(Form("3x7ClusterLr2Eta"));
  an->mini->el_ClCells77_vec->push_back(clusterCellsE);
  an->mini->el_ClCellsEta_vec->push_back(clusterCellsEta);
  
    
 Double_t sumE7x7 =0, sumE3x3=0, sumE3x7=0, sumE37=0, sumE3x5=0;
 Double_t  sumEEta=0, sumEEtaSq=0, weta2  = 0;

if (debug) {
//visual cluster 7x11
/*     std::cout << "-----------------------------CLUSTER 7x11-----------------------------" << std::endl;

  for ( int eta = 0; eta < 7; eta ++ ) {                                                                            
    std::cout << "\n" << std::endl;
    for ( int phi = 0; phi < 11; phi ++ ) {                                                                          
      std::cout << "|" << clusterCellsE[phi+11*eta] << "|";                                                                                      
    }                                                                                                                         
  }   
    std::cout << "\n" << std::endl;

//visual cluster 3x7
     std::cout << "-----------------------------CLUSTER 3x7-----------------------------" << std::endl;

  for ( int eta = 0; eta < 3; eta ++ ) {                                                                            
    std::cout << "\n" << std::endl;
    for ( int phi = 0; phi < 7; phi ++ ) {                                                                          
      std::cout << "|" << clusterCellsE37[phi+7*eta] << "|";                                                                                      
    }                                                                                                                         
  }   
    std::cout << "\n" << std::endl;






//visual Eta cluster 7x11
     std::cout << "-----------------------------CLUSTER 7x11 ETA-----------------------------" << std::endl;

   for ( int eta = 0; eta < 7; eta ++ ) {                                                                            
    std::cout << "\n" << std::endl;
    for ( int phi = 0; phi < 11; phi ++ ) {                                                                          
      std::cout << "|" << clusterCellsEta[phi+11*eta] << "|";                                                                                      
    }                                                                                                                         
  }   
    std::cout << "\n" << std::endl;


//visual cluster 3x7
     std::cout << "-----------------------------CLUSTER 3x7 ETA-----------------------------" << std::endl;

  for ( int eta = 0; eta < 3; eta ++ ) {                                                                            
    std::cout << "\n" << std::endl;
    for ( int phi = 0; phi < 7; phi ++ ) {                                                                          
      std::cout << "|" << clusterCellsEta37[phi+7*eta] << "|";                                                                                      
    }                                                                                                                         
  }   
    std::cout << "\n" << std::endl;

        std::cout << "Cluster 7x11 "<< clusterCellsE[24] << "Cluster 3x7" << clusterCellsE37[0] << std::endl;
*/

}

  //Calculating sumE7x7
  for ( int eta = 0; eta < 7; eta ++ ) {                                                                            
    for ( int phi = 2; phi < 9; phi ++ ) {                                                                          
      sumE7x7 += clusterCellsE[phi+11*eta];                                                                                      
    }                                                                                                                         
  }   
  //Calculating sumE3x3                                                                                                            
  for ( int eta = 0; eta < 3; eta ++ ) {
    for ( int phi = 2; phi < 5 ; phi ++ ) {
      sumE3x3 += clusterCellsE37[phi+7*eta];
    }
  }                                                                                                                        
  //Calculating sumE3x7                                                                                                            
  for ( int eta = 2; eta < 5; eta ++ ) {
    for ( int phi = 2; phi < 9; phi ++ ) {
      sumE3x7 += clusterCellsE[phi+11*eta];
    }
  }  
  
  for (int k = 0; k < (21) ; k++) {
    sumE37 += clusterCellsE37[k];
    //    std::cout << "Cell #" << k << " energy is " << clusterCellsE[k] << std::endl;                                                                                                                   
  }  


  if(debug) {
 std::cout << "SumE7x7: " << sumE7x7 << " SumE3x7: " << sumE3x7 << " SumE3x3: " << sumE3x3 << std::endl;
 std::cout << "SumE3x7 Correct: " << sumE37 << std::endl;
 std::cout << "SumE3x7 Default: " << (an->goodElecs->at(i))->auxdata<float>("E3x7_Lr2")<< std::endl;
       std::cout << "Central cell Eta is: |" << clusterCellsEta[24] << "|" << std::endl;                                                                                      

 }



  //Calculating Weta2
  
   for ( int eta = 2; eta < 5; eta ++ ) {
    for ( int phi = 3; phi < 8; phi ++ ) {
      sumE3x5 += clusterCellsE[phi+11*eta];
      sumEEta += clusterCellsE[phi+11*eta]*clusterCellsEta[phi+11*eta];
      sumEEtaSq += clusterCellsE[phi+11*eta]*clusterCellsEta[phi+11*eta]*clusterCellsEta[phi+11*eta];
    }
  }

   weta2 = sqrt( sumEEtaSq/sumE3x5 - (sumEEta/sumE3x5)*(sumEEta/sumE3x5) );
  
  if (debug) {
  std::cout << "SumE3x5: " << sumE3x5 << " SumEEta: " << sumEEta << " SumEtaSq: " << sumEEtaSq << std::endl;
  std::cout << "Weta2_new is: " << weta2 << " while Weta2 is: " << (an->goodElecs->at(i))->auxdata<float>("weta2") << std::endl;
  std::cout << "Widths2 is: " << (an->goodElecs->at(i))->auxdata<float>("widths2") << std::endl;



  if ( abs((sumE37/sumE7x7) - (an->goodElecs->at(i))->auxdata<float>("Reta")) > 0.01  ) {
      std::cout<<"Discrepancy in Reta:" << abs((sumE37/sumE7x7) - (an->goodElecs->at(i))->auxdata<float>("Reta"))<< std::endl; 
    }
   std::cout << "Reta_new is: " << sumE3x7/sumE7x7 << " while Reta is: " << (an->goodElecs->at(i))->auxdata<float>("Reta") << std::endl;


    if ( abs((sumE3x3/sumE37) - (an->goodElecs->at(i))->auxdata<float>("Rphi")) > 0.01 ) {
      std::cout<<"Stange Rphi:" << abs((sumE3x3/sumE37) - (an->goodElecs->at(i))->auxdata<float>("Rphi")) <<std::endl; 
    }
  std::cout << "Rphi_new is: " << sumE3x3/sumE3x7 << " while Rphi is: " << (an->goodElecs->at(i))->auxdata<float>("Rphi") << std::endl;
   
}

  an->mini->el_Reta_new->push_back(sumE37/sumE7x7);
  an->mini->el_Rphi_new->push_back(sumE3x3/sumE37);
  an->mini->el_weta2_new->push_back(weta2);
  









  
  //--------------------------------------------------------------------
  
  double sumE = 0;
  // Total energy of the electron cluster                                                                    
  for (int k = 0; k < (77) ; k++) {
    sumE += clusterCellsE[k];
   
  }
 // std::cout << "Cluster energy is " << sumE << std::endl;
 //   std::cout << "SumE7x11 Default: " << (an->goodElecs->at(i))->auxdata<float>("E7x11_Lr2")<< std::endl;
  int nphi = 11; 
  int neta = 7;
  //summing energy over current eta column                                   
  
//find MaxElement
float maxE = clusterCellsE[0];
int maxElement =0;
  for (int k = 1; k < (77) ; k++) {
if (maxE < clusterCellsE[k]) {
  maxE = clusterCellsE[k];
  maxElement = k;

}
  }

if (maxElement!= 38)   an->mini->el_f1->push_back(maxElement);

//std::cout << "Cell Number: " << maxElement << std::endl << " MAX ELEMENT ETA: " << (maxElement/11) + 1 << " MAX ELEMENT PHI: " << maxElement%11 +1 << std::endl ;


    std::vector< float> sumEEtaColumns;
    for ( int eta = 0; eta < neta; eta ++ ) {
      float sumEEtaCol = 0;  
      for ( int phi = 0; phi < nphi; phi ++ ) {
	 sumEEtaCol += clusterCellsE[phi+nphi*eta];
      }
      sumEEtaColumns.push_back(sumEEtaCol/sumE);
    }



    // summing energy over current phi column                                 
    
    std::vector< float> sumEPhiColumns;
    for ( int phi = 0; phi < nphi; phi ++ ) {
      float sumEPhiCol = 0; 
      for ( int eta = 0; eta < neta; eta ++ ) {
  
	sumEPhiCol += clusterCellsE[phi + eta*nphi];
      }
      sumEPhiColumns.push_back(sumEPhiCol/sumE);
    }      


	an->mini->el_ClEtaColumns7x11Lr2_vec->push_back(sumEEtaColumns);
	an->mini->el_ClPhiColumns7x11Lr2_vec->push_back(sumEPhiColumns);
	an->mini->el_Cl_SumE7x11Lr2->push_back(sumE);      
	
	
	
	

}
