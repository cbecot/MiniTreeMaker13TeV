#ifndef MINITREEMAKERCOMMON_H
#define MINITREEMAKERCOMMON_H
//#include "MiniTreeMaker.h"
//Output tree variables

#include <string>
#include <map>
#include <TTree.h>


class MiniTreeMakerCommon {

  friend class MiniTreeMaker;
  friend class MiniTreeMaker13TeV;
  friend class Anahelp;
  
  public: 
  MiniTreeMakerCommon(){};
  virtual ~MiniTreeMakerCommon(){};

  private:
  
  bool m_isMC;
  
  
  //eventwise variables
  std::map<std::string, float> *EventWeight;
  float    RunNumber;
  float    EvtNumber;
  float    vxp_z0;
  float    npv;
  float    nintime;
  float    bcid;
  float    lbn;
  float  actual_mu;
  int  n_electrons;
  unsigned int mcChannelNumber;
  bool   passTrigger_el;
  bool   passTrigger_mu;
  
  //jets "just for fun"
  std::vector<float> * jet_pt;
  std::vector<float> * jet_eta;
  std::vector<float> * jet_phi;

  //muon kinematics and variables
  std::vector<float> * mu_pt;
  std::vector<float> * mu_idpt;
  std::vector<float> * mu_eta;
  std::vector<float> * mu_phi;
  std::vector<float> * mu_charge;
  std::vector<float> * mu_d0sig;
  std::vector<float> * mu_delta_z0;
  std::vector< std::map<std::string, float> > * mu_isolation; //string = e.g. ptvarcone30, topoetcone20 (13 TeV analysis)
  std::vector<bool> * mu_isTight;
  std::vector<float> * mu_medium_recosf;
  std::vector<float> * mu_tight_recosf;
  std::vector<float> * mu_ttvasf;
  std::vector<float> * mu_isosf;
  std::vector<float> * mu_trigsf;
  std::vector<float> * mu_trigmceff;
  std::vector<bool> * mu_trigMatch;
  std::vector<bool> * mu_inrecoil;

  //muon systematics
  std::vector< std::map<std::string, float> > * mu_syspt_vec;
  std::vector< std::map<std::string, float> > * mu_sysidpt_vec;
  std::vector< std::map<std::string, float> > * mu_systtvasf_vec;
  std::vector< std::map<std::string, float> > * mu_sys_medium_recosf_vec;
  std::vector< std::map<std::string, float> > * mu_sys_tight_recosf_vec;
  std::vector< std::map<std::string, float> > * mu_sysisosf_vec;
  std::vector< std::map<std::string, float> > * mu_systrigsf_vec;
  std::vector< std::map<std::string, float> > * mu_systrigmceff_vec;

  //electron kinematics and variables
  
  std::vector<float> * el_pt;
  std::vector<float> * el_eta;
  std::vector<float> * el_phi;
  std::vector<float> * el_charge;
  std::vector<float> * el_d0sig;
  std::vector<float> * el_delta_z0;
  std::vector< std::map<std::string, float> > * el_isolation; //string = e.g. ptvarcone20, topoetcone20 (13 TeV analysis)
  std::vector<bool> * el_isTight;  
  std::vector<bool> * el_isMedium;
  std::vector<bool> * el_isLoose;
  std::vector<float> * el_recosf;
  std::vector<float> * el_medium_idsf;
  std::vector<float> * el_tight_idsf;
  std::vector<float> * el_loose_idsf;
  std::vector<float> * el_medium_isosf;
  std::vector<float> * el_tight_isosf;
  std::vector<float> * el_trigsf;
  std::vector<float> * el_trigmceff;
  std::vector<bool> * el_trigMatch;
  std::vector<bool> * el_inrecoil;

  //  std::vector<float> * el_cluster3x7EL1;
  //  std::vector<float> * el_cluster3x7EL2;
  //  std::vector<float> * el_cluster3x7EL3;s
  //  std::vector<float> * el_cluster7x11EPS;
  
  

  
  //  std::vector<float> * el_cluster7x11EL1;

  std::vector<float> *el_weta1;
  std::vector<float> *el_weta2;
  std::vector<float> *el_Reta;
  std::vector<float> *el_Rphi;
  std::vector<float> *el_Eratio;
  std::vector<float> *el_f1;
  std::vector<float> *el_f3;
  std::vector<float> *el_weta2_new;
  std::vector<float> *el_Reta_new;
  std::vector<float> *el_Rphi_new;



  std::vector<int> * el_ClusterSize7x11Lr2;
  std::vector<float> * el_Cl_SumE7x11Lr2;
  std::vector <std::vector<float>> * el_ClEtaColumns7x11Lr2_vec;
  std::vector <std::vector<float>> * el_ClPhiColumns7x11Lr2_vec;

  std::vector<int> * el_ClusterSize3x7Lr2;
  std::vector<float> * el_Cl_SumE3x7Lr2;
  std::vector <std::vector<float>> * el_ClEtaColumns3x7Lr2_vec;
  std::vector <std::vector<float>> * el_ClPhiColumns3x7Lr2_vec;
  std::vector <std::vector<float>> * el_ClCells77_vec;
  std::vector <std::vector<float>> * el_ClCellsEta_vec;


std::vector<int> * el_topoClustersNumber;
std::vector <std::vector<float>> * el_topoClustersE;
std::vector <std::vector<float>> * el_topoClustersEta;
std::vector <std::vector<float>> * el_topoClustersPhi;
std::vector <std::vector<float>> * el_topoClustersPt;

 //  std::vector<float> * el_cluster7x11EL3;
  // std::vector<float> * el_cluster7x11EPS;
  


  //electron systematics
  std::vector< std::map<std::string, float> > * el_syspt_vec;
  std::vector< std::map<std::string, float> > * el_sysrecosf_vec;
  std::vector< std::map<std::string, float> > * el_sys_medium_idsf_vec;
  std::vector< std::map<std::string, float> > * el_sys_medium_isosf_vec;
  std::vector< std::map<std::string, float> > * el_systrigsf_vec;
  std::vector< std::map<std::string, float> > * el_systrigmceff_vec;
  std::vector< std::map<std::string, float> > * el_sys_tight_idsf_vec;
  std::vector< std::map<std::string, float> > * el_sys_tight_isosf_vec;

  //recoil variables
  std::map<std::string, float> *u_pt;
  std::map<std::string, float> *u_phi;
  std::map<std::string, float> *u_sumet;
  int u_ncones_removed;
  std::vector<float> * incl_mu_pt;
  std::vector<float> * incl_mu_eta;
  std::vector<float> * incl_mu_phi;
  std::vector<float> * incl_mu_d0sig;
  std::vector<float> * incl_mu_delta_z0;

  //truth variables
  //truth recoil
  std::map<std::string, float> *tu_pt;
  std::map<std::string, float> *tu_phi;
  std::map<std::string, float> *tu_sumet;
  //truth leptons
  std::vector<float> * tlep_dressed_pt;
  std::vector<float> * tlep_dressed_eta;
  std::vector<float> * tlep_dressed_phi;
  std::vector<float> * tlep_dressed_e;
  std::vector<int> * tlep_dressed_pdgId;
  std::vector<float> * tlep_bare_pt;
  std::vector<float> * tlep_bare_eta;
  std::vector<float> * tlep_bare_phi;
  std::vector<float> * tlep_bare_e;
  std::vector<int> * tlep_bare_pdgId;
  std::vector<float> * tlep_born_pt;
  std::vector<float> * tlep_born_eta;
  std::vector<float> * tlep_born_phi;
  std::vector<float> * tlep_born_e;
  std::vector<int> * tlep_born_pdgId;
  std::vector<float> * tneutrino_pt;
  std::vector<float> * tneutrino_eta;
  std::vector<float> * tneutrino_phi;
  std::vector<float> * tneutrino_e;
  std::vector<int> * tneutrino_pdgId;
  //truth photons
  std::vector<float> * tphoton_pt;
  std::vector<float> * tphoton_eta;
  std::vector<float> * tphoton_phi;
  std::vector<float> * tphoton_e;
  //truth jets
  std::vector<float> * tjet_pt;
  std::vector<float> * tjet_eta;
  std::vector<float> * tjet_phi;
  std::vector<float> * tjet_e;
  //truth boson
  float   tboson_pt;
  float   tboson_eta;
  float   tboson_phi;
  float   tboson_e;
  int   tboson_pdgId;
  //incoming partons
  int tparton_pdgId1;
  int tparton_pdgId2;

  TTree *t;

  void initialiseTree(){
    t = new TTree("treeAnaWZ","Tree for W mass analysis");
    
    t->Branch("EventWeight",&EventWeight);
    t->Branch("RunNumber",&RunNumber);
    t->Branch("EvtNumber",&EvtNumber);
    t->Branch("vxp_z0",&vxp_z0);
    t->Branch("npv",&npv);
    t->Branch("nintime",&nintime);
    t->Branch("bcid",&bcid);
    t->Branch("lbn",&lbn);
    t->Branch("actual_mu",&actual_mu);
    t->Branch("mcChannelNumber",&mcChannelNumber);
    t->Branch("passTrigger_el",&passTrigger_el);
    t->Branch("passTrigger_mu",&passTrigger_mu);
    t->Branch("n_electrons", &n_electrons);

    t->Branch("mu_pt",&mu_pt);
    t->Branch("mu_idpt",&mu_idpt);
    t->Branch("mu_eta",&mu_eta);
    t->Branch("mu_phi",&mu_phi);
    t->Branch("mu_charge",&mu_charge);
    t->Branch("mu_d0sig",&mu_d0sig);
    t->Branch("mu_delta_z0",&mu_delta_z0);
    t->Branch("mu_isolation",&mu_isolation);
    t->Branch("mu_isTight",&mu_isTight);
    t->Branch("mu_medium_recosf",&mu_medium_recosf);
    t->Branch("mu_tight_recosf",&mu_tight_recosf);
    t->Branch("mu_ttvasf",&mu_ttvasf);
    t->Branch("mu_isosf",&mu_isosf);
    t->Branch("mu_trigsf",&mu_trigsf);
    t->Branch("mu_trigmceff",&mu_trigmceff);
    t->Branch("mu_trigMatch",&mu_trigMatch);
    t->Branch("mu_inrecoil",&mu_inrecoil);
    t->Branch("mu_syspt_vec",&mu_syspt_vec);
    t->Branch("mu_sysidpt_vec",&mu_sysidpt_vec);
    t->Branch("mu_sys_medium_recosf_vec",&mu_sys_medium_recosf_vec);
    t->Branch("mu_sys_tight_recosf_vec",&mu_sys_tight_recosf_vec);
    t->Branch("mu_systtvasf_vec",&mu_systtvasf_vec);
    t->Branch("mu_sysisosf_vec",&mu_sysisosf_vec);
    t->Branch("mu_systrigsf_vec",&mu_systrigsf_vec);
    t->Branch("mu_systrigmceff_vec",&mu_systrigmceff_vec);

    t->Branch("el_pt",&el_pt);
    t->Branch("el_eta",&el_eta);
    t->Branch("el_phi",&el_phi);
    t->Branch("el_charge",&el_charge);
    t->Branch("el_d0sig",&el_d0sig);
    t->Branch("el_delta_z0",&el_delta_z0);
    t->Branch("el_isolation",&el_isolation);
    t->Branch("el_isTight",&el_isTight);
    t->Branch("el_isMedium",&el_isMedium);
    t->Branch("el_isLoose",&el_isLoose);

    t->Branch("el_recosf",&el_recosf);
    t->Branch("el_medium_idsf",&el_medium_idsf);
    t->Branch("el_medium_isosf",&el_medium_isosf);
    t->Branch("el_trigsf",&el_trigsf);
    t->Branch("el_trigmceff",&el_trigmceff);
    t->Branch("el_tight_idsf",&el_tight_idsf);
    t->Branch("el_tight_isosf",&el_tight_isosf);
    t->Branch("el_loose_idsf",&el_loose_idsf);

    t->Branch("el_trigMatch",&el_trigMatch);
    t->Branch("el_inrecoil",&el_inrecoil);
    t->Branch("el_syspt_vec",&el_syspt_vec);
    t->Branch("el_sysrecosf_vec",&el_sysrecosf_vec);
    t->Branch("el_sys_medium_idsf_vec",&el_sys_medium_idsf_vec);
    t->Branch("el_sys_medium_isosf_vec",&el_sys_medium_isosf_vec);
    t->Branch("el_systrigsf_vec",&el_systrigsf_vec);
    t->Branch("el_systrigmceff_vec",&el_systrigmceff_vec);
    t->Branch("el_sys_tight_idsf_vec",&el_sys_tight_idsf_vec);
    t->Branch("el_sys_tight_isosf_vec",&el_sys_tight_isosf_vec);

    t->Branch("el_ClusterSize7x11Lr2", &el_ClusterSize7x11Lr2);
    t->Branch("el_Cl_SumE7x11Lr2", &el_Cl_SumE7x11Lr2);
    t->Branch("el_ClEtaColumns7x11Lr2_vec", &el_ClEtaColumns7x11Lr2_vec);
    t->Branch("el_ClPhiColumns7x11Lr2_vec", &el_ClPhiColumns7x11Lr2_vec);

    t->Branch("el_ClusterSize3x7Lr2", &el_ClusterSize3x7Lr2);
    t->Branch("el_Cl_SumE3x7Lr2", &el_Cl_SumE3x7Lr2);
    t->Branch("el_ClEtaColumns3x7Lr2_vec", &el_ClEtaColumns3x7Lr2_vec);
    t->Branch("el_ClPhiColumns3x7Lr2_vec", &el_ClPhiColumns3x7Lr2_vec);
    t->Branch("el_ClCells77_vec", &el_ClCells77_vec);
    t->Branch("el_ClCellsEta_vec", &el_ClCellsEta_vec);

    t->Branch("el_topoClustersNumber", &el_topoClustersNumber);
    t->Branch("el_topoClustersE", &el_topoClustersE);
    t->Branch("el_topoClustersEta", &el_topoClustersEta);
    t->Branch("el_topoClustersPhi", &el_topoClustersPhi);
    t->Branch("el_topoClustersPt", &el_topoClustersPt);



    t->Branch("el_weta1", &el_weta1);
    t->Branch("el_weta2", &el_weta2);
    t->Branch("el_Reta", &el_Reta);
    t->Branch("el_Rphi", &el_Rphi);
    t->Branch("el_Eratio", &el_Eratio);
    t->Branch("el_f1", &el_f1);
    t->Branch("el_f3", &el_f3);

    t->Branch("el_weta2_new", &el_weta2_new);
    t->Branch("el_Reta_new", &el_Reta_new);
    t->Branch("el_Rphi_new", &el_Rphi_new);


    t->Branch("u_pt",&u_pt);
    t->Branch("u_phi",&u_phi);
    t->Branch("u_sumet",&u_sumet);
    t->Branch("u_ncones_removed",&u_ncones_removed);
    //all medium muons with pT > 5 GeV
    t->Branch("incl_mu_pt",&incl_mu_pt);
    t->Branch("incl_mu_eta",&incl_mu_eta);
    t->Branch("incl_mu_phi",&incl_mu_phi);
    t->Branch("incl_mu_d0sig",&incl_mu_d0sig);
    t->Branch("incl_mu_delta_z0",&incl_mu_delta_z0);


    t->Branch("jet_pt",&jet_pt);
    t->Branch("jet_eta",&jet_eta);
    t->Branch("jet_phi",&jet_phi);

    if(m_isMC){
      t->Branch("tu_pt",&tu_pt);
      t->Branch("tu_phi",&tu_phi);
      t->Branch("tu_sumet",&tu_sumet);

      t->Branch("tjet_pt",&tjet_pt);
      t->Branch("tjet_eta",&tjet_eta);
      t->Branch("tjet_phi",&tjet_phi);
      t->Branch("tjet_e",&tjet_e);

      t->Branch("tboson_pt",&tboson_pt);
      t->Branch("tboson_eta",&tboson_eta);
      t->Branch("tboson_phi",&tboson_phi);
      t->Branch("tboson_e",&tboson_e);
      t->Branch("tboson_pdgId",&tboson_pdgId);

      t->Branch("tphoton_pt",&tphoton_pt);
      t->Branch("tphoton_eta",&tphoton_eta);
      t->Branch("tphoton_phi",&tphoton_phi);
      t->Branch("tphoton_e",&tphoton_e);

      t->Branch("tlep_dressed_pt",&tlep_dressed_pt);
      t->Branch("tlep_dressed_eta",&tlep_dressed_eta);
      t->Branch("tlep_dressed_phi",&tlep_dressed_phi);
      t->Branch("tlep_dressed_e",&tlep_dressed_e);
      t->Branch("tlep_dressed_pdgId",&tlep_dressed_pdgId);

      t->Branch("tlep_bare_pt",&tlep_bare_pt);
      t->Branch("tlep_bare_eta",&tlep_bare_eta);
      t->Branch("tlep_bare_phi",&tlep_bare_phi);
      t->Branch("tlep_bare_e",&tlep_bare_e);
      t->Branch("tlep_bare_pdgId",&tlep_bare_pdgId);

      t->Branch("tlep_born_pt",&tlep_born_pt);
      t->Branch("tlep_born_eta",&tlep_born_eta);
      t->Branch("tlep_born_phi",&tlep_born_phi);
      t->Branch("tlep_born_e",&tlep_born_e);
      t->Branch("tlep_born_pdgId",&tlep_born_pdgId);

      t->Branch("tneutrino_pt",&tneutrino_pt);
      t->Branch("tneutrino_eta",&tneutrino_eta);
      t->Branch("tneutrino_phi",&tneutrino_phi);
      t->Branch("tneutrino_e",&tneutrino_e);
      t->Branch("tneutrino_pdgId",&tneutrino_pdgId);

      t->Branch("tparton_pdgId1",&tparton_pdgId1);
      t->Branch("tparton_pdgId2",&tparton_pdgId2);
    }
  }
  public:

  void initVec(){
    //map poitners
    EventWeight = new std::map<std::string, float>();
    u_pt = new std::map<std::string, float>();
    u_phi = new std::map<std::string, float>();
    u_sumet = new std::map<std::string, float>();
  
    jet_pt = new std::vector<float>();
    jet_eta = new std::vector<float>();
    jet_phi = new std::vector<float>();

    mu_pt = new std::vector<float>();
    mu_idpt = new std::vector<float>();
    mu_eta = new std::vector<float>();
    mu_phi = new std::vector<float>();
    mu_charge = new std::vector<float>();
    mu_d0sig = new std::vector<float>();
    mu_delta_z0 = new std::vector<float>();
    mu_isolation = new std::vector< std::map<std::string, float> >();
    mu_isTight = new std::vector<bool>();
    mu_medium_recosf = new std::vector<float>();
    mu_tight_recosf = new std::vector<float>();
    mu_ttvasf = new std::vector<float>();
    mu_isosf = new std::vector<float>();
    mu_trigsf = new std::vector<float>();
    mu_trigmceff = new std::vector<float>();
    mu_trigMatch = new std::vector<bool>();
    mu_inrecoil = new std::vector<bool>();
    mu_syspt_vec = new std::vector< std::map<std::string, float> >();
    mu_sysidpt_vec = new std::vector< std::map<std::string, float> >();
    mu_sys_medium_recosf_vec = new std::vector< std::map<std::string, float> >();
    mu_sys_tight_recosf_vec = new std::vector< std::map<std::string, float> >();
    mu_systtvasf_vec = new std::vector< std::map<std::string, float> >();
    mu_sysisosf_vec = new std::vector< std::map<std::string, float> >();
    mu_systrigsf_vec = new std::vector< std::map<std::string, float> >();
    mu_systrigmceff_vec = new std::vector< std::map<std::string, float> >();

    el_pt = new std::vector<float>();
    el_eta = new std::vector<float>();
    el_phi = new std::vector<float>();
    el_charge = new std::vector<float>();
    el_d0sig = new std::vector<float>();
    el_delta_z0 = new std::vector<float>();
    el_isolation = new std::vector< std::map<std::string, float> >();
    el_isTight = new std::vector<bool>();
    el_isMedium = new std::vector<bool>();
    el_isLoose = new std::vector<bool>();

    el_recosf = new std::vector<float>();
    el_medium_idsf = new std::vector<float>();
    el_medium_isosf = new std::vector<float>();
    el_trigsf = new std::vector<float>();
    el_trigmceff = new std::vector<float>();
    el_tight_idsf = new std::vector<float>();
    el_tight_isosf = new std::vector<float>();
    el_trigMatch = new std::vector<bool>();
    el_inrecoil = new std::vector<bool>();
    el_loose_idsf = new std::vector<float>();

    el_ClusterSize7x11Lr2 = new std::vector<int>();
    el_Cl_SumE7x11Lr2 = new std::vector<float>();
    el_ClEtaColumns7x11Lr2_vec = new std::vector<std::vector<float>>();
    el_ClPhiColumns7x11Lr2_vec = new std::vector<std::vector<float>>();
    el_ClCells77_vec = new std::vector<std::vector<float>>();
    el_ClCellsEta_vec = new std::vector<std::vector<float>>();
    el_ClusterSize3x7Lr2 = new std::vector<int>();
    el_Cl_SumE3x7Lr2 = new std::vector<float>();
    el_ClEtaColumns3x7Lr2_vec = new std::vector<std::vector<float>>();
    el_ClPhiColumns3x7Lr2_vec = new std::vector<std::vector<float>>();

    el_topoClustersNumber = new std::vector<int>();
    el_topoClustersE = new std::vector<std::vector<float>>();
    el_topoClustersEta = new std::vector<std::vector<float>>();
    el_topoClustersPhi = new std::vector<std::vector<float>>();
    el_topoClustersPt = new std::vector<std::vector<float>>();


    el_weta1 = new std::vector<float>;
    el_weta2 = new std::vector<float>;
    el_Reta = new std::vector<float>;
    el_Rphi = new std::vector<float>;
    el_Eratio = new std::vector<float>;
    el_f1 = new std::vector<float>;
    el_f3 = new std::vector<float>;
    el_weta2_new = new std::vector<float>;
    el_Reta_new = new std::vector<float>;
    el_Rphi_new = new std::vector<float>;



    el_syspt_vec = new std::vector< std::map<std::string, float> >();
    el_sysrecosf_vec = new std::vector< std::map<std::string, float> >();
    el_sys_medium_idsf_vec = new std::vector< std::map<std::string, float> >();
    el_sys_medium_isosf_vec = new std::vector< std::map<std::string, float> >();
    el_systrigsf_vec = new std::vector< std::map<std::string, float> >();
    el_systrigmceff_vec = new std::vector< std::map<std::string, float> >();
    el_sys_tight_idsf_vec = new std::vector< std::map<std::string, float> >();
    el_sys_tight_isosf_vec = new std::vector< std::map<std::string, float> >();

    incl_mu_pt = new std::vector<float>();
    incl_mu_eta = new std::vector<float>();
    incl_mu_phi = new std::vector<float>();
    incl_mu_d0sig = new std::vector<float>();
    incl_mu_delta_z0 = new std::vector<float>();

    if(m_isMC){
      tu_pt = new std::map<std::string, float>();
      tu_phi = new std::map<std::string, float>();
      tu_sumet = new std::map<std::string, float>();
  

      tlep_dressed_pt = new std::vector<float>();
      tlep_dressed_eta = new std::vector<float>();
      tlep_dressed_phi = new std::vector<float>();
      tlep_dressed_e = new std::vector<float>();
      tlep_dressed_pdgId = new std::vector<int>();
      tlep_bare_pt = new std::vector<float>();
      tlep_bare_eta = new std::vector<float>();
      tlep_bare_phi = new std::vector<float>();
      tlep_bare_e = new std::vector<float>();
      tlep_bare_pdgId = new std::vector<int>();
      tlep_born_pt = new std::vector<float>();
      tlep_born_eta = new std::vector<float>();
      tlep_born_phi = new std::vector<float>();
      tlep_born_e = new std::vector<float>();
      tlep_born_pdgId = new std::vector<int>();
      tneutrino_pt = new std::vector<float>();
      tneutrino_eta = new std::vector<float>();
      tneutrino_phi = new std::vector<float>();
      tneutrino_e = new std::vector<float>();
      tneutrino_pdgId = new std::vector<int>();
      tphoton_pt = new std::vector<float>();
      tphoton_eta = new std::vector<float>();
      tphoton_phi = new std::vector<float>();
      tphoton_e = new std::vector<float>();
      tjet_pt = new std::vector<float>();
      tjet_eta = new std::vector<float>();
      tjet_phi = new std::vector<float>();
      tjet_e = new std::vector<float>();
    }
  };
  void clearVec(){
    
    jet_pt->clear();
    jet_eta->clear();
    jet_phi->clear();
    mcChannelNumber = 0;
    EventWeight->clear();
    mu_pt->clear();
    mu_idpt->clear();
    mu_eta->clear();
    mu_phi->clear();
    mu_charge->clear();
    mu_d0sig->clear();
    mu_delta_z0->clear();
    mu_isolation->clear();
    mu_isTight->clear();
    mu_medium_recosf->clear();
    mu_tight_recosf->clear();
    mu_ttvasf->clear();
    mu_isosf->clear();
    mu_trigsf->clear();
    mu_trigmceff->clear();
    mu_trigMatch->clear();
    mu_inrecoil->clear();
    mu_syspt_vec->clear();
    mu_sysidpt_vec->clear();
    mu_sys_medium_recosf_vec->clear();
    mu_sys_tight_recosf_vec->clear();
    mu_systtvasf_vec->clear();
    mu_sysisosf_vec->clear();
    mu_systrigsf_vec->clear();
    mu_systrigmceff_vec->clear();

    el_pt->clear();
    el_eta->clear();
    el_phi->clear();
    el_charge->clear();
    el_d0sig->clear();
    el_delta_z0->clear();
    el_isolation->clear();
    el_isTight->clear();
    el_isMedium->clear();
    el_isLoose->clear();

    el_recosf->clear();
    el_medium_idsf->clear();
    el_medium_isosf->clear();
    el_trigsf->clear();
    el_trigmceff->clear();
    el_tight_idsf->clear();
    el_loose_idsf->clear();

    el_tight_isosf->clear();
    el_trigMatch->clear();
    el_inrecoil->clear();

    el_ClusterSize7x11Lr2->clear();
    el_Cl_SumE7x11Lr2->clear();
    el_ClEtaColumns7x11Lr2_vec->clear();
    el_ClPhiColumns7x11Lr2_vec->clear();
    el_ClCells77_vec->clear();
    el_ClCellsEta_vec->clear();

    el_topoClustersNumber->clear();
    el_topoClustersE->clear();
    el_topoClustersEta->clear();
    el_topoClustersPhi->clear();
    el_topoClustersPt->clear();


    el_ClusterSize3x7Lr2->clear();
    el_Cl_SumE3x7Lr2->clear();
    el_ClEtaColumns3x7Lr2_vec->clear();
    el_ClPhiColumns3x7Lr2_vec->clear();
    
    el_weta1->clear();
    el_weta2->clear();
    el_Rphi->clear();
    el_Reta->clear();
    el_weta2_new->clear();
    el_Rphi_new->clear();
    el_Reta_new->clear();

    el_Eratio->clear();
    el_f1->clear();
    el_f3->clear();    

    el_syspt_vec->clear();
    el_sysrecosf_vec->clear();
    el_sys_medium_idsf_vec->clear();
    el_sys_medium_isosf_vec->clear();
    el_systrigsf_vec->clear();
    el_systrigmceff_vec->clear();
    el_sys_tight_idsf_vec->clear();
    el_sys_tight_isosf_vec->clear();

    u_pt->clear();
    u_ncones_removed = -1;
    u_phi->clear();
    u_sumet->clear();
    incl_mu_pt->clear();
    incl_mu_eta->clear();
    incl_mu_phi->clear();
    incl_mu_d0sig->clear();
    incl_mu_delta_z0->clear();

    if(m_isMC){
      tu_pt->clear();
      tu_phi->clear();
      tu_sumet->clear();

      tlep_dressed_pt->clear();
      tlep_dressed_eta->clear();
      tlep_dressed_phi->clear();
      tlep_dressed_e->clear();
      tlep_dressed_pdgId->clear();
      tlep_bare_pt->clear();
      tlep_bare_eta->clear();
      tlep_bare_phi->clear();
      tlep_bare_e->clear();
      tlep_bare_pdgId->clear();
      tlep_born_pt->clear();
      tlep_born_eta->clear();
      tlep_born_phi->clear();
      tlep_born_e->clear();
      tlep_born_pdgId->clear();
      tneutrino_pt->clear();
      tneutrino_eta->clear();
      tneutrino_phi->clear();
      tneutrino_e->clear();
      tneutrino_pdgId->clear();
      tphoton_pt->clear();
      tphoton_eta->clear();
      tphoton_phi->clear();
      tphoton_e->clear();
      tjet_pt->clear();
      tjet_eta->clear();
      tjet_phi->clear();
      tjet_e->clear();
      tparton_pdgId1 = -9999;
      tparton_pdgId2 = -9999;
      tboson_pt = -9999;
      tboson_eta = -9999;
      tboson_phi = -9999;
      tboson_e = -9999;
      tboson_pdgId = -9999;

    }
  };
  public:
    TTree *getTree(){
      return t;
    };
    void initialise (bool isMC) {
      m_isMC = isMC;
      initVec();
      clearVec();
      initialiseTree();
    };
    void finaliseEvent(){
    
      clearVec();
    
    };
};

#endif //MINITREEMAKERCOMMON_H

