
#ifndef ANAHELPER_H
#define ANAHELPER_H

#include <iostream>
#include <string>
#include <vector>
#include <map>
#include <unordered_map>

#ifndef __MAKECINT__
#include "xAODJet/JetContainer.h"
#include "xAODMuon/MuonContainer.h"
#include "xAODEgamma/ElectronContainer.h"
#endif // not __MAKECINT__

#include "xAODMuon/MuonAuxContainer.h"
#include "xAODJet/JetAuxContainer.h"
#include "xAODEgamma/ElectronAuxContainer.h"
#include "xAODMissingET/MissingETContainer.h"
#include "xAODMissingET/MissingETAuxContainer.h"
#include "xAODCutFlow/CutBookkeeper.h"
#include "xAODCutFlow/CutBookkeeperContainer.h"

#include "TFile.h"
#include "TString.h"
#include "TTree.h"
#include "TH1.h"
#include "MiniTreeMaker13TeV/MCtruth.h"

class Anahelp{
	friend class MiniTreeMaker13TeV;
	friend class initfinal;
public:
	Anahelp(){};
	virtual ~Anahelp(){};
	void fillTree(){t->Fill();};

private:
	TFile *fout;
	bool m_isMC;
	TTree *t;
	// Histograms
	TH1F* m_cutFlow;
		//this is for powheg initial SoW
	unsigned int nprocessed;
	float wtgenPow;
	bool passTrigger_el;
	bool passTrigger_mu;
	//The containers with selected objects

	xAOD::MuonContainer* goodMuons; //!
	xAOD::MuonContainer* inclMuons; //!
	xAOD::ElectronContainer* goodElecs; //!
	xAOD::ElectronContainer* goodFwdElecs; //!
	xAOD::JetContainer* goodJets; //!
	xAOD::MuonAuxContainer *goodMuonsAux; //!
	xAOD::MuonAuxContainer *inclMuonsAux; //!
	xAOD::ElectronAuxContainer *goodElecsAux; //!
	xAOD::ElectronAuxContainer *goodFwdElecsAux; //!
	xAOD::JetAuxContainer* goodJetsAux; //!
	xAOD::MissingETContainer* MetContainer; //!
	xAOD::MissingETAuxContainer* MetAuxContainer; //!
	std::vector< bool > NomSelElecs;
	std::vector< bool > NomSelMuons;
	std::vector< bool > NomSelJets;

		//tree variables
	std::map<TString,double> _values;

	std::vector<float> *jet_pt_vec, *jet_eta_vec, *jet_phi_vec;
	std::vector<float> *incl_mu_pt_vec, *incl_mu_eta_vec, *incl_mu_phi_vec;
	std::vector<float> *mu_pt_vec, *mu_eta_vec, *mu_phi_vec, *mu_charge_vec, *mu_ptvarcone30_vec, *mu_topoetcone20_vec, *mu_d0sig_vec, *mu_deltaz0new_vec;
	std::vector<float> *el_pt_vec, *el_eta_vec, *el_phi_vec, *el_charge_vec, *el_ptvarcone20_vec, *el_topoetcone20_vec, *el_d0sig_vec, *el_deltaz0new_vec;
	std::vector<float> *el_sf_vec ,*el_trigMatch_vec, *el_trigsf_vec, *el_trigmceff_vec, *el_trigsf_temp, *el_trigmceff_temp;	
	std::vector<float> *mu_sf_vec ,*mu_trigMatch_vec, *mu_trigsf_vec, *mu_trigmceff_vec, *mu_trigsf_temp, *mu_trigmceff_temp;
	std::vector<int> *el_id_rg, *el_iso_rg, *el_reco_rg, *el_trig_rg;
	std::vector<float> *tlep_dressed_pt_vec, *tlep_dressed_eta_vec, *tlep_dressed_phi_vec, *tlep_dressed_e_vec, *tlep_dressed_pdgId_vec; 
	std::vector<float> *tlep_bare_pt_vec, *tlep_bare_eta_vec, *tlep_bare_phi_vec, *tlep_bare_e_vec, *tlep_bare_pdgId_vec; 
	std::vector<float> *tlep_born_pt_vec, *tlep_born_eta_vec, *tlep_born_phi_vec, *tlep_born_e_vec, *tlep_born_pdgId_vec; 
	std::vector<float> *tneutrino_pt_vec, *tneutrino_eta_vec, *tneutrino_phi_vec, *tneutrino_e_vec,*tneutrino_pdgID_vec;		
	std::vector<float> *tphoton_pt_vec, *tphoton_eta_vec, *tphoton_phi_vec, *tphoton_e_vec;
	std::vector<float> *tjet_pt_vec, *tjet_eta_vec, *tjet_phi_vec, *tjet_e_vec;
	std::vector<float> *tboson_pt_vec, *tboson_eta_vec, *tboson_phi_vec, *tboson_e_vec, *tboson_pdgId_vec;
	std::vector< std::pair<float,std::string> > *el_syspt_vec, *mu_syspt_vec;
	std::vector< std::pair<float,std::string> > *el_syssf_vec, *mu_syssf_vec , *el_systrigsf_vec, *el_systrigmceff_vec, *mu_systrigsf_vec, *mu_systrigmceff_vec;
	std::vector<float> *tparton_pt_vec, *tparton_eta_vec, *tparton_phi_vec, *tparton_e_vec, *tparton_pdgId_vec; 
	std::vector< std::pair<float,std::string> > *MET_sys_vec;
	std::vector< std::pair<float,std::string> > *MET_phi_sys_vec;
	std::vector< std::pair<float,std::string> > *upfo_pt_sys_vec, *upfo_eta_sys_vec, *upfo_phi_sys_vec, *upfo_e_sys_vec;
	std::vector< std::pair<float,std::string> > *upfovor_pt_sys_vec, *upfovor_eta_sys_vec, *upfovor_phi_sys_vec, *upfovor_e_sys_vec;
	std::vector< std::pair<float,std::string> > *upfovorsp_pt_sys_vec, *upfovorsp_eta_sys_vec, *upfovorsp_phi_sys_vec, *upfovorsp_e_sys_vec;
	float MET,MET_phi,Sumet;
	float fwdel_pt, fwdel_eta, fwdel_phi, fwdel_sf;
	float upfo_pt, upfo_eta, upfo_phi, upfo_e;
	float upfovor_pt, upfovor_eta, upfovor_phi, upfovor_e;
	float upfovorsp_pt, upfovorsp_eta, upfovorsp_phi, upfovorsp_e;

public:

////////////////////////////////////
//
//
////////////////////////////////////
	void initialise(TString foutname, bool ismc){
		m_isMC = ismc;
		InitialiseVec();
		clearVec();
		fout = new TFile(foutname, "RECREATE");
		InitialiseTree();
		InitialiseCutflow();

		goodMuons = new xAOD::MuonContainer();
		inclMuons = new xAOD::MuonContainer();
		goodElecs = new xAOD::ElectronContainer();
		goodFwdElecs = new xAOD::ElectronContainer();
		goodJets = new xAOD::JetContainer();
		MetContainer    = new xAOD::MissingETContainer();

		goodMuonsAux = new xAOD::MuonAuxContainer();
		inclMuonsAux = new xAOD::MuonAuxContainer();
		goodElecsAux = new xAOD::ElectronAuxContainer();
		goodFwdElecsAux = new xAOD::ElectronAuxContainer();
		goodJetsAux = new xAOD::JetAuxContainer();
		MetAuxContainer = new xAOD::MissingETAuxContainer();

		goodMuons->setStore( goodMuonsAux );
		inclMuons->setStore( inclMuonsAux );
		goodElecs->setStore( goodElecsAux );
		goodFwdElecs->setStore( goodFwdElecsAux );
		goodJets->setStore( goodJetsAux );
		MetContainer->setStore( MetAuxContainer );

	//for Powheg sum of weights
		wtgenPow=0.;
		nprocessed = 0;
		return;
	}

	void getTruth(MCtruth* mctruth){
		std::pair<int, int> pdgid = mctruth->getPDGID();
		_values["pdg1"] = pdgid.first;
		_values["pdg2"] = pdgid.second;
		if(mctruth->getNeutrino() != nullptr ){
			tneutrino_pt_vec->push_back( mctruth->getNeutrino()->pt());
			tneutrino_e_vec->push_back( mctruth->getNeutrino()->e());
			tneutrino_eta_vec->push_back( mctruth->getNeutrino()->eta());
			tneutrino_phi_vec->push_back( mctruth->getNeutrino()->phi());
			tneutrino_pdgID_vec->push_back( mctruth->getNeutrino()->pdgId());
		}
		tboson_pt_vec->push_back( mctruth->getBoson()->pt());
		tboson_e_vec->push_back( mctruth->getBoson()->e());
		tboson_eta_vec->push_back( mctruth->getBoson()->eta());
		tboson_phi_vec->push_back( mctruth->getBoson()->phi());
		tboson_pdgId_vec->push_back( mctruth->getBoson()->pdgId());

		for (auto& it : mctruth->getBareLeps()) {
			tlep_bare_pt_vec->push_back( (it)->pt());
			tlep_bare_e_vec->push_back( (it)->e());
			tlep_bare_eta_vec->push_back( (it)->eta());
			tlep_bare_phi_vec->push_back( (it)->phi());
			tlep_bare_pdgId_vec->push_back( (it)->pdgId());
		}
		for (auto& it : mctruth->getBornLeps()) {
			tlep_born_pt_vec->push_back( (it)->pt());
			tlep_born_e_vec->push_back( (it)->e());
			tlep_born_eta_vec->push_back( (it)->eta());
			tlep_born_phi_vec->push_back( (it)->phi());
			tlep_born_pdgId_vec->push_back( (it)->pdgId());
		}
		for (auto& it : mctruth->getDressedLeps()) {
			tlep_dressed_pt_vec->push_back( (it)->pt());
			tlep_dressed_e_vec->push_back( (it)->e());
			tlep_dressed_eta_vec->push_back( (it)->eta());
			tlep_dressed_phi_vec->push_back( (it)->phi());
			tlep_dressed_pdgId_vec->push_back( (it)->pdgId());
		}
		for (auto& it : mctruth->getPhotons()) {
			tphoton_pt_vec->push_back( (it)->pt());
			tphoton_e_vec->push_back( (it)->e());
			tphoton_eta_vec->push_back( (it)->eta());
			tphoton_phi_vec->push_back( (it)->phi());
		}
		for (auto& it : mctruth->getJets()) {
			tjet_pt_vec->push_back( (it)->pt());
			tjet_e_vec->push_back( (it)->e());
			tjet_eta_vec->push_back( (it)->eta());
			tjet_phi_vec->push_back( (it)->phi());
		}

	}




// Initialise the tree
	void InitialiseTree() {
		TString treeName="treeAnaWZ";
		fout->cd();
		t = new TTree(treeName,"Tree for NI calibration");
		t->Branch("EventWeight",&_values["EventWeight"]);
		t->Branch("RunNumber",&_values["RunNumber"]);
		t->Branch("EvtNumber",&_values["EvtNumber"]);
		t->Branch("npv",&_values["npv"]);
		t->Branch("npuv",&_values["npuv"]);
		t->Branch("zvertex",&_values["zvertex"]);
		t->Branch("average_mu",&_values["avmu"]);
		t->Branch("jet_n",&_values["jet_n"]);
		t->Branch("jet_pt",&jet_pt_vec);
		t->Branch("jet_eta",&jet_eta_vec);
		t->Branch("jet_phi",&jet_phi_vec);

		t->Branch("mu_n",&_values["mu_n"]);
		t->Branch("mu_pt",&mu_pt_vec);
		t->Branch("mu_eta",&mu_eta_vec);
		t->Branch("mu_phi",&mu_phi_vec);
		t->Branch("mu_sf",&mu_sf_vec);	
		t->Branch("mu_charge",&mu_charge_vec);

		t->Branch("incl_mu_pt",&incl_mu_pt_vec);
		t->Branch("incl_mu_eta",&incl_mu_eta_vec);
		t->Branch("incl_mu_phi",&incl_mu_phi_vec);

		t->Branch("mu_trigMatch",&mu_trigMatch_vec);
		t->Branch("mu_syspt",&mu_syspt_vec);	
		t->Branch("mu_syssf",&mu_syssf_vec);	
		t->Branch("mu_trigmceff",&mu_trigmceff_vec);
		t->Branch("mu_trigsf",&mu_trigsf_vec);
		t->Branch("mu_systrigsf",&mu_systrigsf_vec);
		t->Branch("el_n",&_values["el_n"]);
		t->Branch("el_pt",&el_pt_vec);
		t->Branch("el_eta",&el_eta_vec);
		t->Branch("el_phi",&el_phi_vec);
		t->Branch("el_sf",&el_sf_vec);	
		t->Branch("fwdel_pt",&fwdel_pt);
		t->Branch("fwdel_eta",&fwdel_eta);
		t->Branch("fwdel_phi",&fwdel_phi);
		t->Branch("fwdel_sf",&fwdel_sf);	
		t->Branch("el_charge",&el_charge_vec);	
		t->Branch("el_trigMatch",&el_trigMatch_vec);	
		t->Branch("el_trigmceff",&el_trigmceff_vec);	
		t->Branch("el_trigsf",&el_trigsf_vec);	
		t->Branch("el_syspt",&el_syspt_vec);	
		t->Branch("el_syssf",&el_syssf_vec);	
		t->Branch("el_systrigsf",&el_systrigsf_vec);	
		t->Branch("met",&MET);
		t->Branch("met_phi",&MET_phi);	
		t->Branch("sumet",&Sumet);	
		t->Branch("met_sys",&MET_sys_vec);	
		t->Branch("met_phi_sys",&MET_phi_sys_vec);	
		t->Branch("passTrigger_el",&passTrigger_el);
		t->Branch("passTrigger_mu",&passTrigger_mu);
		t->Branch("upfo_pt",&upfo_pt);
		t->Branch("upfo_eta",&upfo_eta);
		t->Branch("upfo_phi",&upfo_phi);
		t->Branch("upfo_e",&upfo_e);
		t->Branch("upfovor_pt",&upfovor_pt);
		t->Branch("upfovor_eta",&upfovor_eta);
		t->Branch("upfovor_phi",&upfovor_phi);
		t->Branch("upfovor_e",&upfovor_e);
		t->Branch("upfovorsp_pt",&upfovorsp_pt);
		t->Branch("upfovorsp_eta",&upfovorsp_eta);
		t->Branch("upfovorsp_phi",&upfovorsp_phi);
		t->Branch("upfovorsp_e",&upfovorsp_e);
		t->Branch("upfo_pt_sys",&upfo_pt_sys_vec);
		t->Branch("upfo_eta_sys",&upfo_eta_sys_vec);
		t->Branch("upfo_phi_sys",&upfo_phi_sys_vec);
		t->Branch("upfo_e_sys",&upfo_e_sys_vec);
		t->Branch("upfovor_pt_sys",&upfovor_pt_sys_vec);
		t->Branch("upfovor_eta_sys",&upfovor_eta_sys_vec);
		t->Branch("upfovor_phi_sys",&upfovor_phi_sys_vec);
		t->Branch("upfovor_e_sys",&upfovor_e_sys_vec);
		t->Branch("upfovorsp_pt_sys",&upfovorsp_pt_sys_vec);
		t->Branch("upfovorsp_eta_sys",&upfovorsp_eta_sys_vec);
		t->Branch("upfovorsp_phi_sys",&upfovorsp_phi_sys_vec);
		t->Branch("upfovorsp_e_sys",&upfovorsp_e_sys_vec);

		if(m_isMC){

			t->Branch("tjet_pt",&tjet_pt_vec);
			t->Branch("tjet_eta",&tjet_eta_vec);
			t->Branch("tjet_phi",&tjet_phi_vec);
			t->Branch("tjet_e",&tjet_e_vec);

			t->Branch("tboson_pt",&tboson_pt_vec);
			t->Branch("tboson_eta",&tboson_eta_vec);
			t->Branch("tboson_phi",&tboson_phi_vec);
			t->Branch("tboson_e",&tboson_e_vec);
			t->Branch("tboson_pdgId",&tboson_pdgId_vec);

			t->Branch("tphoton_pt",&tphoton_pt_vec);
			t->Branch("tphoton_eta",&tphoton_eta_vec);
			t->Branch("tphoton_phi",&tphoton_phi_vec);
			t->Branch("tphoton_e",&tphoton_e_vec);

			t->Branch("tlep_dressed_pt",&tlep_dressed_pt_vec);
			t->Branch("tlep_dressed_eta",&tlep_dressed_eta_vec);
			t->Branch("tlep_dressed_phi",&tlep_dressed_phi_vec);
			t->Branch("tlep_dressed_e",&tlep_dressed_e_vec);
			t->Branch("tlep_dressed_pdgId",&tlep_dressed_pdgId_vec);

			t->Branch("tlep_bare_pt",&tlep_bare_pt_vec);
			t->Branch("tlep_bare_eta",&tlep_bare_eta_vec);
			t->Branch("tlep_bare_phi",&tlep_bare_phi_vec);
			t->Branch("tlep_bare_e",&tlep_bare_e_vec);
			t->Branch("tlep_bare_pdgId",&tlep_bare_pdgId_vec);

			t->Branch("tlep_born_pt",&tlep_born_pt_vec);
			t->Branch("tlep_born_eta",&tlep_born_eta_vec);
			t->Branch("tlep_born_phi",&tlep_born_phi_vec);
			t->Branch("tlep_born_e",&tlep_born_e_vec);
			t->Branch("tlep_born_pdgId",&tlep_born_pdgId_vec);

			t->Branch("tneutrino_pt",&tneutrino_pt_vec);
			t->Branch("tneutrino_eta",&tneutrino_eta_vec);
			t->Branch("tneutrino_phi",&tneutrino_phi_vec);
			t->Branch("tneutrino_e",&tneutrino_e_vec);
			t->Branch("tneutrino_pdgID",&tneutrino_pdgID_vec);

			t->Branch("tparton_pdgId1",&_values["pdg1"]);
			t->Branch("tparton_pdgId2",&_values["pdg2"]);
		}
		t->Branch("mu_ptvarcone30",&mu_ptvarcone30_vec);
		t->Branch("mu_topoetcone20",&mu_topoetcone20_vec);
		t->Branch("mu_d0sig",&mu_d0sig_vec);
		t->Branch("mu_deltaz0new",&mu_deltaz0new_vec);
		t->Branch("el_ptvarcone20",&el_ptvarcone20_vec);
		t->Branch("el_topoetcone20",&el_topoetcone20_vec);
		t->Branch("el_d0sig",&el_d0sig_vec);
		t->Branch("el_deltaz0new",&el_deltaz0new_vec);
	}

	void InitialiseVec(){
		if(m_isMC){

			tjet_pt_vec = new std::vector<float>();
			tjet_eta_vec = new std::vector<float>();
			tjet_phi_vec = new std::vector<float>();
			tjet_e_vec = new std::vector<float>();

			tboson_pt_vec = new std::vector<float>();
			tboson_eta_vec = new std::vector<float>();
			tboson_phi_vec = new std::vector<float>();
			tboson_e_vec = new std::vector<float>();
			tboson_pdgId_vec = new std::vector<float>();

			tphoton_pt_vec = new std::vector<float>();
			tphoton_eta_vec = new std::vector<float>();
			tphoton_phi_vec = new std::vector<float>();
			tphoton_e_vec = new std::vector<float>();

			tlep_dressed_pt_vec = new std::vector<float>();
			tlep_dressed_eta_vec = new std::vector<float>();
			tlep_dressed_phi_vec = new std::vector<float>();
			tlep_dressed_e_vec = new std::vector<float>();
			tlep_dressed_pdgId_vec = new std::vector<float>();

			tlep_bare_pt_vec = new std::vector<float>();
			tlep_bare_eta_vec = new std::vector<float>();
			tlep_bare_phi_vec = new std::vector<float>();
			tlep_bare_e_vec = new std::vector<float>();
			tlep_bare_pdgId_vec = new std::vector<float>();

			tlep_born_pt_vec = new std::vector<float>();
			tlep_born_eta_vec = new std::vector<float>();
			tlep_born_phi_vec = new std::vector<float>();
			tlep_born_e_vec = new std::vector<float>();
			tlep_born_pdgId_vec = new std::vector<float>();

			tneutrino_pt_vec = new std::vector<float>();
			tneutrino_eta_vec = new std::vector<float>();
			tneutrino_phi_vec = new std::vector<float>();
			tneutrino_e_vec = new std::vector<float>();
			tneutrino_pdgID_vec = new std::vector<float>();


		}

		jet_pt_vec = new std::vector<float>();
		jet_eta_vec = new std::vector<float>();
		jet_phi_vec = new std::vector<float>();
		mu_pt_vec = new std::vector<float>();
		mu_eta_vec = new std::vector<float>();
		mu_phi_vec = new std::vector<float>();
		incl_mu_pt_vec = new std::vector<float>();
		incl_mu_eta_vec = new std::vector<float>();
		incl_mu_phi_vec = new std::vector<float>();
		mu_charge_vec = new std::vector<float>();
		mu_sf_vec = new std::vector<float>();
		mu_trigMatch_vec = new std::vector<float>();
		mu_syspt_vec = new std::vector< std::pair<float,std::string> >();
		mu_syssf_vec = new std::vector< std::pair<float,std::string> >();
		mu_trigsf_vec = new std::vector<float>();
		mu_trigmceff_vec = new std::vector<float>();
		mu_systrigsf_vec = new std::vector< std::pair<float,std::string> >();
		mu_systrigmceff_vec = new std::vector< std::pair<float,std::string> >();		

		el_pt_vec = new std::vector<float>();
		el_eta_vec = new std::vector<float>();
		el_phi_vec = new std::vector<float>();
		el_charge_vec = new std::vector<float>();
		el_sf_vec = new std::vector<float>();
		el_trigsf_vec = new std::vector<float>();
		el_trigmceff_vec = new std::vector<float>();
		el_trigMatch_vec = new std::vector<float>();
		el_syspt_vec = new std::vector< std::pair<float,std::string> >();
		el_syssf_vec = new std::vector< std::pair<float,std::string> >();
		el_systrigsf_vec = new std::vector< std::pair<float,std::string> >();
		el_systrigmceff_vec = new std::vector< std::pair<float,std::string> >();
		el_id_rg = new std::vector<int>();
		el_iso_rg = new std::vector<int>();
		el_reco_rg = new std::vector<int>();
		el_trig_rg = new std::vector<int>();

		MET_sys_vec = new std::vector< std::pair<float,std::string> >();
		MET_phi_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfo_pt_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfo_eta_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfo_phi_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfo_e_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovor_pt_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovor_eta_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovor_phi_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovor_e_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovorsp_pt_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovorsp_eta_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovorsp_phi_sys_vec = new std::vector< std::pair<float,std::string> >();
		upfovorsp_e_sys_vec = new std::vector< std::pair<float,std::string> >();
		mu_ptvarcone30_vec = new std::vector<float>();
		mu_topoetcone20_vec = new std::vector<float>();
		mu_d0sig_vec = new std::vector<float>();
		mu_deltaz0new_vec = new std::vector<float>();
		el_ptvarcone20_vec = new std::vector<float>();
		el_topoetcone20_vec = new std::vector<float>();
		el_d0sig_vec = new std::vector<float>();
		el_deltaz0new_vec = new std::vector<float>();
	}
	void clearVec(){
		jet_pt_vec->clear();
		jet_eta_vec->clear();
		jet_phi_vec->clear();
		mu_ptvarcone30_vec->clear();
		mu_topoetcone20_vec->clear();
		mu_d0sig_vec->clear();
		mu_deltaz0new_vec->clear();
		el_ptvarcone20_vec->clear();
		el_topoetcone20_vec->clear();
		el_d0sig_vec->clear();
		el_deltaz0new_vec->clear();
		mu_pt_vec->clear();
		mu_eta_vec->clear();
		mu_phi_vec->clear();
		incl_mu_pt_vec->clear();
		incl_mu_eta_vec->clear();
		incl_mu_phi_vec->clear();
		mu_charge_vec->clear();
		mu_sf_vec->clear();
		mu_trigMatch_vec->clear();
		mu_syspt_vec->clear();
		mu_syssf_vec->clear();
		mu_trigsf_vec->clear();
		mu_trigmceff_vec->clear();
		mu_systrigmceff_vec->clear();
		mu_systrigsf_vec->clear();

		el_pt_vec->clear();
		el_eta_vec->clear();
		el_phi_vec->clear();
		el_charge_vec->clear();
		el_trigsf_vec->clear();
		el_trigmceff_vec->clear();
		el_sf_vec->clear();
		el_trigMatch_vec->clear();
		el_syspt_vec->clear();
		el_syssf_vec->clear();
		el_systrigmceff_vec->clear();
		el_systrigsf_vec->clear();
		el_id_rg->clear();
		el_trig_rg->clear();
		el_reco_rg->clear();
		el_iso_rg->clear();

		MET_sys_vec->clear();
		MET_phi_sys_vec->clear();

		upfo_pt_sys_vec->clear();
		upfo_eta_sys_vec->clear();
		upfo_phi_sys_vec->clear();
		upfo_e_sys_vec->clear();
		upfovor_pt_sys_vec->clear();
		upfovor_eta_sys_vec->clear();
		upfovor_phi_sys_vec->clear();
		upfovor_e_sys_vec->clear();
		upfovorsp_pt_sys_vec->clear();
		upfovorsp_eta_sys_vec->clear();
		upfovorsp_phi_sys_vec->clear();
		upfovorsp_e_sys_vec->clear();

		if(m_isMC){
			tjet_pt_vec->clear();
			tjet_eta_vec->clear();
			tjet_phi_vec->clear();
			tjet_e_vec->clear();

			tboson_pt_vec->clear();
			tboson_eta_vec->clear();
			tboson_phi_vec->clear();
			tboson_e_vec->clear();
			tboson_pdgId_vec->clear();

			tphoton_pt_vec->clear();
			tphoton_eta_vec->clear();
			tphoton_phi_vec->clear();
			tphoton_e_vec->clear();

			tlep_dressed_pt_vec->clear();
			tlep_dressed_eta_vec->clear();
			tlep_dressed_phi_vec->clear();
			tlep_dressed_e_vec->clear();
			tlep_dressed_pdgId_vec->clear();

			tlep_bare_pt_vec->clear();
			tlep_bare_eta_vec->clear();
			tlep_bare_phi_vec->clear();
			tlep_bare_e_vec->clear();
			tlep_bare_pdgId_vec->clear();

			tlep_born_pt_vec->clear();
			tlep_born_eta_vec->clear();
			tlep_born_phi_vec->clear();
			tlep_born_e_vec->clear();
			tlep_born_pdgId_vec->clear();

			tneutrino_pt_vec->clear();
			tneutrino_eta_vec->clear();
			tneutrino_phi_vec->clear();
			tneutrino_e_vec->clear();
			tneutrino_pdgID_vec->clear();

		}
	}

	void clearCont(){
		goodMuons->clear();
		inclMuons->clear();
		goodElecs->clear();
		goodFwdElecs->clear();
		goodJets->clear();
		MetContainer->clear();
	}


   // Initialise the cutflow
	void InitialiseCutflow() {
		m_cutFlow = new TH1F("CutFlow","Cut flow",50,0,50);

		m_cutFlow->GetXaxis()->SetBinLabel(1,"All events");
		m_cutFlow->GetXaxis()->SetBinLabel(2,"GRL");
		m_cutFlow->GetXaxis()->SetBinLabel(3,"LAr error");
		m_cutFlow->GetXaxis()->SetBinLabel(4,"PV");
		m_cutFlow->GetXaxis()->SetBinLabel(5,"Syst tools");
		m_cutFlow->GetXaxis()->SetBinLabel(6,">=1 good lepton");
		m_cutFlow->GetXaxis()->SetBinLabel(7,"el_channel or mu_channel");
		m_cutFlow->GetXaxis()->SetBinLabel(8,"pass trigger");
		m_cutFlow->GetXaxis()->SetBinLabel(40,"Initial Nevts");
		m_cutFlow->GetXaxis()->SetBinLabel(41,"Initial SoW");
		m_cutFlow->GetXaxis()->SetBinLabel(42,"Initial SoW2");

	}

	void AddCutflow(TFile *f , xAOD::TEvent& event, TString daodtype){

		//this is here for historical reasons, when producing personal derivations with specific format	
		//TH1F *h = (TH1F*) f->Get("CutFlow");
		TH1F *h = NULL;
		//simple case
		if(h != NULL){
			m_cutFlow -> Add(h);
		}
		else { //difficult case
			const xAOD::CutBookkeeperContainer* cutflows(0); 
			event.retrieveMetaInput(cutflows, "CutBookkeepers");
		// First, let's find the smallest cycle number,
		// i.e., the original first processing step/cycle
			TString tmps = daodtype + "Kernel";
			std::string derivationName = tmps.Data();
			int minCycle = 10000; 
			for (const xAOD::CutBookkeeper* cutflow: *cutflows) {
				if( !cutflow->name().empty() && minCycle > cutflow->cycle() ){ minCycle = cutflow->cycle(); }
			}
		// Now, let's actually find the right one that contains all the needed info...	
			const xAOD::CutBookkeeper* cutflowAll(0);
			const xAOD::CutBookkeeper* cutflowDAOD(0);

			int maxCycle = -1; 
			for (const xAOD::CutBookkeeper* cutflow: *cutflows) {
				if( cutflow->inputStream() == "StreamAOD"&& cutflow->name() == "AllExecutedEvents" && maxCycle < cutflow->cycle() ){
					maxCycle = cutflow->cycle();
					cutflowAll = cutflow;
				}
				if(cutflow->name() ==derivationName){
					cutflowDAOD = cutflow;  //not needed for now...maybe useful one day
				}
			}
			uint64_t nEventsProcessed  = cutflowAll->nAcceptedEvents();
		//this is to avoid huge weights in Powheg due to xsection
			double SumOfWeights        = cutflowAll->sumOfEventWeights();
			double SumOfWeightsSquared = cutflowAll->sumOfEventWeightsSquared();
			m_cutFlow ->SetBinContent(40, nEventsProcessed +  m_cutFlow ->GetBinContent(40) );
			m_cutFlow ->SetBinContent(41,  SumOfWeights  +  m_cutFlow ->GetBinContent(41));
			m_cutFlow ->SetBinError(41,  sqrt(SumOfWeightsSquared+  (m_cutFlow ->GetBinError(41) * m_cutFlow->GetBinError(41))) );
		}
	}



	void WriteTree(bool isPowheg) {
		fout -> cd();
		if(isPowheg){
			m_cutFlow->SetBinContent(41, m_cutFlow ->GetBinContent(41) / (wtgenPow/nprocessed) );
			m_cutFlow->SetBinError(41, m_cutFlow ->GetBinError(41) / ( wtgenPow *wtgenPow/(nprocessed * nprocessed) ) );
			m_cutFlow->SetBinContent(42, m_cutFlow ->GetBinContent(42) / ( wtgenPow *wtgenPow/(nprocessed * nprocessed) ) );
		}
		m_cutFlow->Write();

		std::cout << "Writing Tree..." << std::endl;
		//fout -> cd();
		t->Write();
		fout->Close();
		delete fout;
		//delete m_cutFlow;
	}
// Finalise
	void FinaliseEvent(xAOD::TEvent& m_event) {
		NomSelElecs.clear();
		NomSelMuons.clear();
		NomSelJets.clear();
	//	m_event.record( goodMuons, "goodMuons"  );
	//	m_event.record( goodElecs, "goodElecs"  );
	//	m_event.record( goodJets, "goodJets"  );
	}

};
#endif
