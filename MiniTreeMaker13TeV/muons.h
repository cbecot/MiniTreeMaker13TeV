
#ifndef MUONS_H
#define MUONS_H

// xAOD ROOT access
#include "xAODRootAccess/Init.h"
#include "xAODRootAccess/TEvent.h"
#include "xAODRootAccess/TStore.h"
#include "xAODEventInfo/EventInfo.h"
#include "xAODCore/ShallowCopy.h"
#include "xAODBase/IParticle.h"
#include "xAODBase/IParticleContainer.h"
#include "xAODBase/IParticleHelpers.h"
#ifndef __MAKECINT__
#include "xAODMuon/MuonContainer.h"
#endif // not __MAKECINT__
//Tools
#include "MuonSelectorTools/MuonSelectionTool.h"
#include "MuonEfficiencyCorrections/MuonEfficiencyScaleFactors.h"
#include "MuonMomentumCorrections/MuonCalibrationAndSmearingTool.h"
#include "MuonEfficiencyCorrections/MuonTriggerScaleFactors.h"
#include "IsolationCorrections/IIsolationCorrectionTool.h"
#include "IsolationSelection/IsolationSelectionTool.h"
#include "xAODTracking/TrackParticlexAODHelpers.h"
#include "AsgTools/AnaToolHandle.h"
#include "MiniTreeMaker13TeV/trigger.h"

namespace CP{
  class MuonCalibrationAndSmearingTool; // this tool lives in the namespace CP             
  class MuonEfficiencyScaleFactors;
  class MuonSelectionTool;
  class MuonTriggerScaleFactors;
  class IsolationSelectionTool;
}

class muons {
  friend class MiniTreeMaker13TeV;
  friend class initfinal;
  public:
  muons();
  virtual ~muons();

  //virtual void finalise();

  int StandardMuonSelection(xAOD::TEvent& m_event,  const xAOD::EventInfo& eventInfo, xAOD::MuonContainer* mucont,  xAOD::MuonContainer* inclMuons, asg::AnaToolHandle<Trig::IMatchingTool> m_muTrigMatch);
  int AntiIsoMuonSelection(xAOD::TEvent& m_event,  const xAOD::EventInfo& eventInfo, xAOD::MuonContainer* mucont,  xAOD::MuonContainer* inclMuons, asg::AnaToolHandle<Trig::IMatchingTool> m_muTrigMatch);
  void InitialiseMuonTools(bool isMC);

  double m_muonMaxEta; 
  int m_muonQuality;
  float m_muonMinPt; 
  float m_muonMinPtIncl; 
  float m_muonFinalMinPt; 
  TString m_muonMCTrigMatch;
  std::vector<TString> muontrigmatchnames; //!

  TString m_MuToolTTVAWP;   
  TString m_MuToolTTVACalib;  
  TString m_MuToolEffiWP;   
  TString m_MuToolEffiCalib;  
  TString m_MuToolIsoWP;    
  bool m_MuToolCalibAndSmearStatComb;  
  TString m_MuToolTrigEffMuQual;  
  TString m_MuToolCalibAndSmearYear;
  TString m_MuToolCalibAndSmearAlgo;
  TString m_MuToolCalibAndSmearType;
  TString m_MuToolCalibAndSmearRel;
  bool m_MuToolCalibAndSmearSagittaCorr;
  TString m_MuToolCalibAndSmearSagittaRel;
  bool m_MuToolCalibAndSmearSagittaMCDist;
  TString m_IsolToolMuonWP;
  

  private:
  bool m_antiIso;
  bool m_isMC, m_do5TeV;
  CP::MuonCalibrationAndSmearingTool *m_muonCalibrationAndSmearingTool; 
  CP::MuonEfficiencyScaleFactors *m_effi_medium_corr;//! 
  CP::MuonEfficiencyScaleFactors *m_effi_tight_corr;//! 
  CP::MuonEfficiencyScaleFactors *m_iso_corr; //!
  CP::MuonSelectionTool *m_muonSelection;//!
  CP::MuonSelectionTool *m_muonInclSelection;//!
  CP::MuonTriggerScaleFactors *m_muTrigEff;//!
  CP::MuonEfficiencyScaleFactors *m_TTVA_effi_corr;//!
  //CP::TightTrackVertexAssociationTool* trkmuver;

  //for both mu and el
  CP::IsolationSelectionTool* m_isolationSelectionTool;

  unsigned int ngoodmu;

};
#endif
