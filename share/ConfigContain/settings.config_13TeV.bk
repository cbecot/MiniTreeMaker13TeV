#####
##
#	config file for dxaod2tree analysis code
#	author :
#	fabrice.balli@cern.ch
#	tairan.xu@cern.ch
##
#####

## Outfile name
fileOutName: OUTDIR/outTree.root

## Dump settings
DumpConfigSettings:	0

## Analysis setting
DoSystematicElecs:	false
DoSystematicMuons:	false
Systematic:	""
DoAntiIso: false
DoJpsi:  false

## 0 : Znunu cuts
## 1 : Zll optimised cuts
WhichAnalysis:	0

PileUpReweighting: 0

DoAcceptance:	false
DoTruth:	true
UseTruth:	0
RecoilElecAtLeastMedium: true
RecoilElecAtLeastTight: false
RecoilMuonAtLeastTight: false


## Lepton preselection acceptance and trigger cuts
MuonMaxEta:	2.4
MuonQuality:	0
MuonMinPt:	18000.
MuonMinRecoilPt:	20000.
MuonMinPtIncl:	5000.
MuonFinalMinPt:	24000.
MuonTrigMatch:	HLT_mu14
MuonMCTrigMatch:	HLT_mu14

ElecMaxEta:	2.47
ElecMinPt:	20000.
ElecMinRecoilPt:	20000.
ElecMinEtaCrack:	1.37
ElecMaxEtaCrack:	1.52
ElecFinalMinPt:	24000.
ElecTrigMatch:	HLT_e15_lhloose_nod0_L1EM12
ElecMCTrigMatch:	HLT_e15_lhloose_nod0_L1EM12
ElecPtMaxTrig:	150000.

## PU and GRL 
LumiCalcFile:	/../../source/MiniTreeMaker13TeV/share/ilumicalc_histograms_None_341294-341649_OflLumi-13TeV-001.root
PUConfFile:	/../../source/MiniTreeMaker13TeV/share/13tevPRW.root
PUUnrepresentedDataAction:	2
PUDataScaleFactor: 	1
PUDefaultChannel:	361106

GRLlist:	/../../source/MiniTreeMaker13TeV/share/data17_13TeV.periodN_DetStatus-v98-pro21-16_Unknown_PHYS_StandardGRL_All_Good_25ns_ignore_GLOBAL_LOWMU.xml
GRLPassThrough:	false

## Jet tools initialisation
JetAlgo:			AntiKt4EMTopo
JetConfig:  			JES_MC16Recommendation_Nov2017.config
JetCalibSeq:  			JetArea_Residual_Origin_EtaJES_GSC

JetCleaningCutLevel: 		LooseBad
JetCleaningDoUgly: 		false

JERPlotFileName: 		JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root
JERCollectionName:		AntiKt4EMTopoJets

JetSmearingApplyNominal: 	false
JetSmearingSystematicMode: 	Full

JesUncerJetDefinition:		AntiKt4EMTopo
JesUncerMCType:			MC15
JesUncerConfigFile:		JES_2015/Moriond2016/JES2015_AllNuisanceParameters.config

## Muon tools initialisation
MuToolTTVAWP:		TTVA
MuToolTTVACalib:	170916_Rel21PreRec
MuToolEffiWP:		Medium
MuToolEffiCalib:	170916_Rel21PreRec
MuToolIsoWP:		FixedCutLooseIso
MuToolCalibAndSmearStatComb:  false
MuToolTrigEffMuQual:	Medium
MuToolCalibAndSmearYear:	Data16
MuToolCalibAndSmearAlgo:	muons
MuToolCalibAndSmearType:	q_pT
MuToolCalibAndSmearRel:	Recs2016_15_07
MuToolCalibAndSmearSagittaCorr: false
MuToolCalibAndSmearSagittaRel: sagittaBiasDataAll_25_07_17
MuToolCalibAndSmearSagittaMCDist: true


## Elec tools initialisation
ElToolRecoCorrFile:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.RecoTrk.root
ElToolLLHCorrFile_medium:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.MediumLLH_d0z0_v13.root
ElToolIsoCorrFile_medium:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/isolation/efficiencySF.Isolation.MediumLLH_d0z0_v13_isolFixedCutLoose.root
ElToolLLHCorrFile_tight:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/offline/efficiencySF.offline.TightLLH_d0z0_v13.root
ElToolIsoCorrFile_tight:	ElectronEfficiencyCorrection/2015_2017/rel21.2/Summer2017_Prerec_v1/isolation/efficiencySF.Isolation.TightLLH_d0z0_v13_isolFixedCutLoose.root
ElToolTrigCorrFile:	ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v3/trigger/efficiencySF.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e24_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v11_isolFixedCutLoose.root
ElToolTrigEffCorrFile:	ElectronEfficiencyCorrection/2015_2016/rel20.7/ICHEP_June2016_v3/trigger/efficiency.SINGLE_E_2015_e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose_2016_e24_lhtight_nod0_ivarloose_OR_e60_lhmedium_nod0_OR_e140_lhloose_nod0.TightLLH_d0z0_v11_isolFixedCutLoose.root
ElToolForcedataType: 	1
ElToolCalibAndSmearESModel:	es2017_R21_PRE
ElToolCalibAndSmearDecorrModel:	FULL_v1
ElToolLlhPVCont:	PrimaryVertices
ElToolLlhLooseConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodLooseOfflineConfig2017_Smooth.conf
ElToolLlhMediumConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodMediumOfflineConfig2017_Smooth.conf
ElToolLlhTightConfigFile:	ElectronPhotonSelectorTools/offline/mc16_20170828/ElectronLikelihoodTightOfflineConfig2017_Smooth.conf


## Isolation tool for both el and mu initialisation 
IsolToolMuonWP:	FixedCutLoose
IsolToolElecWP: FixedCutLoose

## trigger decision tool initialisation
TrigDecisionKey:	xTrigDecision

## trigger chains to get trigger decision
passTriggerElecData:	HLT_e15_lhloose_nod0_L1EM12
passTriggerElecMC:	HLT_e15_lhloose_nod0_L1EM12
passTriggerMuonData:	HLT_mu14
passTriggerMuonMC:	HLT_mu14

## MET initialisation
METJetSelection:	Tight


#you have to end with an additional line as TEnv does not read the last one...
