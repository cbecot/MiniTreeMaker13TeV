#####
##
#	config file for dxaod2tree analysis code
#	author :
#	fabrice.balli@cern.ch
#	tairan.xu@cern.ch
##
#####

## Outfile name
fileOutName: outTree.root

## Dump settings
DumpConfigSettings:	1

## Analysis setting
DoSystematic:	false
Systematic:	""
DoMultiJet: true
## 0 : Znunu cuts
## 1 : Zll optimised cuts
WhichAnalysis:	0

PileUpReweighting: 0

DoAcceptance:	false
DoTruth:	false
UseTruth:	0

## Lepton preselection acceptance and trigger cuts
MuonMaxEta:	2.4
MuonQuality:	1
MuonMinPt:	18000.
MuonMinRecoilPt:	20000.
MuonFinalMinPt:	24000.
MuonTrigMatch:	HLT_mu20_iloose_L1MU15,HLT_mu50
MuonMCTrigMatch:	HLT_mu20_iloose_L1MU15_OR_HLT_mu50

ElecMaxEta:	2.47
ElecMinPt:	20000.
ElecMinRecoilPt:	20000.
ElecMinEtaCrack:	1.37
ElecMaxEtaCrack:	1.52
ElecFinalMinPt:	25000.
ElecTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM20VH,HLT_e120_lhloose
ElecMCTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM18VH,HLT_e120_lhloose
ElecPtMaxTrig:	150000.

## PU and GRL 
LumiCalcFile:	/../WZtreeMaker/share/ilumicalc_histograms_None_276262-284484.root
PUConfFile:	/../WZtreeMaker/share/mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root,/../WZtreeMaker/share/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root
PUUnrepresentedDataAction:	2
PUDataScaleFactor: 	1/1.16
PUDefaultChannel:	361106

GRLlist:	/../WZtreeMaker/share/data15_13TeV.periodAllYear_DetStatus-v73-pro19-08_DQDefects-00-01-02_PHYS_StandardGRL_All_Good_25ns.xml
GRLPassThrough:	false

## Jet tools initialisation
JetAlgo:			AntiKt4EMTopo
JetConfig:  			JES_MC15Prerecommendation_April2015.config
JetCalibSeq:  			JetArea_Residual_EtaJES_GSC

JetCleaningCutLevel: 		LooseBad
JetCleaningDoUgly: 		false

JERPlotFileName: 		JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root
JERCollectionName:		AntiKt4EMTopoJets

JetSmearingApplyNominal: 	false
JetSmearingSystematicMode: 	Full

JesUncerJetDefinition:		AntiKt4EMTopo
JesUncerMCType:			MC15
JesUncerConfigFile:		JES_2015/Prerec/PrerecJES2015_AllNuisanceParameters_25ns.config

## Muon tools initialisation
MuToolTTVAWP:		TTVA
MuToolTTVACalib:	Data15_allPeriods_260116
MuToolEffiWP:		Medium
MuToolEffiCalib:	Data15_allPeriods_260116
MuToolIsoWP:		GradientIso
MuToolTrigEffRunNum:	276262
MuToolTrigEffMuQual:	Medium
MuToolCalibAndSmearYear:	Data15
MuToolCalibAndSmearAlgo:	muons
MuToolCalibAndSmearType:	q_pT
MuToolCalibAndSmearRel:	Recs2016_01_19

## Elec tools initialisation
ElToolRecoCorrFile:	ElectronEfficiencyCorrection/efficiencySF.offline.RecoTrk.2015.13TeV.rel20p0.25ns.v04.root
ElToolLLHCorrFile:	ElectronEfficiencyCorrection/efficiencySF.offline.MediumLLH_d0z0.2015.13TeV.rel20p0.25ns.v04.root
ElToolIsoCorrFile:	ElectronEfficiencyCorrection/efficiencySF.Isolation.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolTrigCorrFile:	ElectronEfficiencyCorrection/efficiencySF.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolTrigEffCorrFile:	ElectronEfficiencyCorrection/efficiency.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolForcedataType: 	1
ElToolCalibAndSmearESModel:	es2015PRE
ElToolCalibAndSmearDecorrModel:	FULL_v1
ElToolLlhPVCont:	PrimaryVertices
ElToolLlhConfigFile:	ElectronPhotonSelectorTools/offline/mc15_20150712/ElectronLikelihoodMediumOfflineConfig2015.conf

## Isolation tool for both el and mu initialisation 
IsolToolMuonWP:	Gradient
IsolToolElecWP:	Gradient

## trigger decision tool initialisation
TrigDecisionKey:	xTrigDecision

## trigger chains to get trigger decision
passTriggerElecData:	HLT_e24_lhmedium_L1EM20VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerElecMC:	HLT_e24_lhmedium_L1EM18VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerMuonData:	HLT_mu20_iloose_L1MU15,HLT_mu50
passTriggerMuonMC:	HLT_mu20_iloose_L1MU15,HLT_mu50

## MET initialisation
METJetSelection:	Tight


#you have to end with an additional line as TEnv does not read the last one...
