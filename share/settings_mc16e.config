#####
##
#	config file for dxaod2tree analysis code
#	author :
#	fabrice.balli@cern.ch
#	tairan.xu@cern.ch
##
#####

## Outfile name
fileOutName: outTree.root

## Dump settings
DumpConfigSettings:	1

## Analysis setting
DoSystematic:	false
Systematic:	""
DoMultiJet: false
## 0 : Znunu cuts
## 1 : Zll optimised cuts
WhichAnalysis:	0

PileUpReweighting: 1

DoAcceptance:	false
DoTruth:	false
UseTruth:	0

PassThrough:    true

## Lepton preselection acceptance and trigger cuts
MuonMaxEta:	2.4
MuonQuality:	1
MuonMinPt:	15000.
MuonFinalMinPt:	25000.
MuonTrigMatch:	HLT_mu20_iloose_L1MU15,HLT_mu50
MuonMCTrigMatch:	HLT_mu20_iloose_L1MU15_OR_HLT_mu50

ElecMaxEta:	2.37
ElecMinPt:	15000.
ElecMinEtaCrack:	1.37
ElecMaxEtaCrack:	1.52
ElecFinalMinPt:	25000.
ElecTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM20VH,HLT_e120_lhloose
ElecMCTrigMatch:	HLT_e60_lhmedium,HLT_e24_lhmedium_L1EM18VH,HLT_e120_lhloose
ElecPtMaxTrig:	150000.

## PU and GRL 
LumiCalcFile:	/data/MiniTreeMaker13TeV/ilumicalc_histograms_None_348885-364292_OflLumi-13TeV-010.root
#PUConfFile:	/../WZtreeMaker/share/mc15_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root,/../WZtreeMaker/share/mc15_13TeV.361107.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zmumu.merge.AOD.e3601_s2576_s2132_r7267_r6282_PRWmc15b.root
PUConfFile:	/data/MiniTreeMaker13TeV/mc16_13TeV.361106.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Zee.deriv.NTUP_PILEUP.e3601_e5984_s3126_r10724_r10726_p3604.prw.root 
PUUnrepresentedDataAction:	2
PUDataScaleFactor: 	1/1.16
PUDefaultChannel:	361106

GRLlist:	/data/MiniTreeMaker13TeV/data18_13TeV.periodAllYear_DetStatus-v105-pro22-13_Unknown_PHYS_StandardGRL_All_Good_25ns_Triggerno17e33prim.xml
GRLPassThrough:	false

## Jet tools initialisation
JetAlgo:			AntiKt4EMTopo
JetConfig:  			JES_2015dataset_recommendation_Feb2016.config
JetCalibSeq:  			JetArea_Residual_Origin_EtaJES_GSC

JetCleaningCutLevel: 		LooseBad
JetCleaningDoUgly: 		false

JERPlotFileName: 		JetResolution/Prerec2015_xCalib_2012JER_ReducedTo9NP_Plots_v2.root
JERCollectionName:		AntiKt4EMTopoJets

JetSmearingApplyNominal: 	false
JetSmearingSystematicMode: 	Full

JesUncerJetDefinition:		AntiKt4EMTopo
JesUncerMCType:			MC15
JesUncerConfigFile:		JES_2015/Moriond2016/JES2015_AllNuisanceParameters.config

## Muon tools initialisation
MuToolTTVAWP:		TTVA
MuToolTTVACalib:	160527_Rel20_7
MuToolEffiWP:		Medium
MuToolEffiCalib:	160527_Rel20_7
MuToolIsoWP:		GradientIso
MuToolTrigEffRunNum:	286361
MuToolTrigEffMuQual:	Medium
MuToolCalibAndSmearYear:	Data15
MuToolCalibAndSmearAlgo:	muons
MuToolCalibAndSmearType:	q_pT
MuToolCalibAndSmearRel:	PreRecs2016_05_23

## Elec tools initialisation
ElToolRecoCorrFile:	ElectronEfficiencyCorrection/efficiencySF.offline.RecoTrk.2015.13TeV.rel20p0.25ns.v04.root
ElToolLLHCorrFile:	ElectronEfficiencyCorrection/efficiencySF.offline.MediumLLH_d0z0.2015.13TeV.rel20p0.25ns.v04.root
ElToolIsoCorrFile:	ElectronEfficiencyCorrection/efficiencySF.Isolation.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolTrigCorrFile:	ElectronEfficiencyCorrection/efficiencySF.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolTrigEffCorrFile:	ElectronEfficiencyCorrection/efficiency.e24_lhmedium_L1EM20VH_OR_e60_lhmedium_OR_e120_lhloose.MediumLLH_d0z0_v8_isolGradient.2015.13TeV.rel20p0.25ns.v04.root
ElToolForcedataType: 	1
ElToolCalibAndSmearESModel:	es2015cPRE_res_improved
ElToolCalibAndSmearDecorrModel:	FULL_v1
ElToolLlhPVCont:	PrimaryVertices
ElToolLlhConfigFile:	ElectronPhotonSelectorTools/offline/mc15_20160512/ElectronLikelihoodMediumOfflineConfig2016_Smooth.conf

## Isolation tool for both el and mu initialisation 
IsolToolMuonWP:	Gradient
IsolToolElecWP:	Gradient

## trigger decision tool initialisation
TrigDecisionKey:	xTrigDecision

## trigger chains to get trigger decision
passTriggerElecData:	HLT_e24_lhmedium_L1EM20VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerElecMC:	HLT_e24_lhmedium_L1EM18VH,HLT_e60_lhmedium,HLT_e120_lhloose
passTriggerMuonData:	HLT_mu20_iloose_L1MU15,HLT_mu50
passTriggerMuonMC:	HLT_mu20_iloose_L1MU15,HLT_mu50

## MET initialisation
METJetSelection:	Tight


#you have to end with an additional line as TEnv does not read the last one...
